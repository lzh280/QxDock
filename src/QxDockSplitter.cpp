/**
 * Copyright (C) 2023 maminjie <canpool@163.com>
 * Copyright (C) 2017 Uwe Kindler
 * SPDX-License-Identifier: LGPL-2.1
 **/

#include "QxDockSplitter.h"
#include "QxDockAreaWidget.h"

#include <QChildEvent>
#include <QDebug>
#include <QVariant>

QX_BEGIN_NAMESPACE

/**
 * Private dock splitter data
 */
struct DockSplitterPrivate {
    DockSplitter *_this;
    int VisibleContentCount = 0;

    DockSplitterPrivate(DockSplitter *_public) : _this(_public)
    {
    }
};

DockSplitter::DockSplitter(QWidget *parent) : QSplitter(parent), d(new DockSplitterPrivate(this))
{
    setProperty("dock-splitter", QVariant(true));
    setChildrenCollapsible(false);
}

DockSplitter::DockSplitter(Qt::Orientation orientation, QWidget *parent)
    : QSplitter(orientation, parent), d(new DockSplitterPrivate(this))
{
}

DockSplitter::~DockSplitter()
{
    QX_DOCK_PRINT("~DockSplitter");
    delete d;
}

bool DockSplitter::hasVisibleContent() const
{
    // TODO Cache or precalculate this to speed up
    for (int i = 0; i < count(); ++i) {
        if (!widget(i)->isHidden()) {
            return true;
        }
    }

    return false;
}

QWidget *DockSplitter::firstWidget() const
{
    return (count() > 0) ? widget(0) : nullptr;
}

QWidget *DockSplitter::lastWidget() const
{
    return (count() > 0) ? widget(count() - 1) : nullptr;
}

bool DockSplitter::isResizingWithContainer() const
{
    for (auto area : findChildren<DockAreaWidget *>()) {
        if (area->isCentralWidgetArea()) {
            return true;
        }
    }

    return false;
}

QX_END_NAMESPACE
