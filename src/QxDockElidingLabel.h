/**
 * Copyright (C) 2023 maminjie <canpool@163.com>
 * Copyright (C) 2017-2018 Uwe Kindler
 * SPDX-License-Identifier: LGPL-2.1
 **/
#pragma once

#include "QxDockGlobal.h"
#include <QLabel>

QX_BEGIN_NAMESPACE

struct DockElidingLabelPrivate;

/**
 * A QLabel that supports eliding text.
 * Because the functions setText() and text() are no virtual functions setting
 * and reading the text via a pointer to the base class QLabel does not work
 * properly
 */
class QX_DOCK_EXPORT DockElidingLabel : public QLabel
{
    Q_OBJECT
private:
    DockElidingLabelPrivate *d;
    friend struct DockElidingLabelPrivate;
protected:
    virtual void mouseReleaseEvent(QMouseEvent *event) override;
    virtual void resizeEvent(QResizeEvent *event) override;
    virtual void mouseDoubleClickEvent(QMouseEvent *ev) override;
public:
    using Super = QLabel;

    DockElidingLabel(QWidget *parent = 0, Qt::WindowFlags f = Qt::WindowFlags());
    DockElidingLabel(const QString &text, QWidget *parent = 0, Qt::WindowFlags f = Qt::WindowFlags());
    virtual ~DockElidingLabel();

    /**
     * Returns the text elide mode.
     * The default mode is ElideNone
     */
    Qt::TextElideMode elideMode() const;

    /**
     * Sets the text elide mode
     */
    void setElideMode(Qt::TextElideMode mode);

    /**
     * This function indicates whether the text on this label is currently elided
     */
    bool isElided() const;
public:   // reimplements QLabel ----------------------------------------------
    virtual QSize minimumSizeHint() const override;
    virtual QSize sizeHint() const override;
    void setText(const QString &text);
    QString text() const;

Q_SIGNALS:
    /**
     * This signal is emitted if the user clicks on the label (i.e. pressed
     * down then released while the mouse cursor is inside the label)
     */
    void clicked();

    /**
     * This signal is emitted if the user does a double click on the label
     */
    void doubleClicked();

    /**
     * This signal is emitted when isElided() state of this label is changed
     */
    void elidedChanged(bool elided);
};   // class DockElidingLabel

QX_END_NAMESPACE

