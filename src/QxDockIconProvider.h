/**
 * Copyright (C) 2023 maminjie <canpool@163.com>
 * Copyright (C) 2017-2019 Uwe Kindler
 * SPDX-License-Identifier: LGPL-2.1
 **/
#pragma once

#include "QxDockGlobal.h"
#include <QIcon>

QX_BEGIN_NAMESPACE

struct DockIconProviderPrivate;

/**
 * This object provides all icons that are required by the advanced docking
 * system.
 * The IconProvider enables the user to register custom icons in case using
 * stylesheets is not an option.
 */
class QX_DOCK_EXPORT DockIconProvider
{
private:
    DockIconProviderPrivate *d;   ///< private data (pimpl)
    friend struct DockIconProviderPrivate;
public:
    /**
     * Default Constructor
     */
    DockIconProvider();

    /**
     * Virtual Destructor
     */
    virtual ~DockIconProvider();

    /**
     * The function returns a custom icon if one is registered and a null Icon
     * if no custom icon is registered
     */
    QIcon customIcon(eIcon IconId) const;

    /**
     * Registers a custom icon for the given IconId
     */
    void registerCustomIcon(eIcon IconId, const QIcon &icon);
};   // class IconProvider

QX_END_NAMESPACE
