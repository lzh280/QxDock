VERSION = 1.0.0

DEFINES += QX_VERSION_DOCK=$$VERSION
DEFINES += QT_DEPRECATED_WARNINGS

windows {
    # MinGW
    *-g++* {
        QMAKE_CXXFLAGS += -Wall -Wextra -pedantic
    }
    # MSVC
    *-msvc* {
                QMAKE_CXXFLAGS += /utf-8
        }
}

RESOURCES += $$PWD/dock.qrc

HEADERS += \
    $$PWD/QxDockGlobal.h \
    $$PWD/QxDockAreaWidget.h \
    $$PWD/QxDockAreaTabBar.h \
    $$PWD/QxDockContainerWidget.h \
    $$PWD/QxDockManager.h \
    $$PWD/QxDockWidget.h \
    $$PWD/QxDockWidgetTab.h \
    $$PWD/QxDockStateReader.h \
    $$PWD/QxDockFloatingContainer.h \
    $$PWD/QxDockFloatingDragPreview.h \
    $$PWD/QxDockOverlay.h \
    $$PWD/QxDockSplitter.h \
    $$PWD/QxDockAreaTitleBarPrivate.h \
    $$PWD/QxDockAreaTitleBar.h \
    $$PWD/QxDockElidingLabel.h \
    $$PWD/QxDockIconProvider.h \
    $$PWD/QxDockFactory.h  \
    $$PWD/QxDockFocusController.h \
    $$PWD/QxDockAutoHideContainer.h \
    $$PWD/QxDockAutoHideSideBar.h \
    $$PWD/QxDockAutoHideTab.h \
    $$PWD/QxDockPushButton.h \
    $$PWD/QxDockResizeHandle.h


SOURCES += \
    $$PWD/QxDockGlobal.cpp \
    $$PWD/QxDockAreaWidget.cpp \
    $$PWD/QxDockAreaTabBar.cpp \
    $$PWD/QxDockContainerWidget.cpp \
    $$PWD/QxDockManager.cpp \
    $$PWD/QxDockWidget.cpp \
    $$PWD/QxDockStateReader.cpp \
    $$PWD/QxDockWidgetTab.cpp \
    $$PWD/QxDockFloatingContainer.cpp \
    $$PWD/QxDockFloatingDragPreview.cpp \
    $$PWD/QxDockOverlay.cpp \
    $$PWD/QxDockSplitter.cpp \
    $$PWD/QxDockAreaTitleBar.cpp \
    $$PWD/QxDockElidingLabel.cpp \
    $$PWD/QxDockIconProvider.cpp \
    $$PWD/QxDockFactory.cpp \
    $$PWD/QxDockFocusController.cpp \
    $$PWD/QxDockAutoHideContainer.cpp \
    $$PWD/QxDockAutoHideSideBar.cpp \
    $$PWD/QxDockAutoHideTab.cpp \
    $$PWD/QxDockPushButton.cpp \
    $$PWD/QxDockResizeHandle.cpp


unix:!macx {
HEADERS += $$PWD/linux/QxDockFloatingWidgetTitleBar.h
SOURCES += $$PWD/linux/QxDockFloatingWidgetTitleBar.cpp
LIBS += -lxcb
QT += gui-private
}
