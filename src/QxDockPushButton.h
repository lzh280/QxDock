/**
 * Copyright (C) 2023 maminjie <canpool@163.com>
 * Copyright (C) 2022 Uwe Kindler
 * SPDX-License-Identifier: LGPL-2.1
 **/
#pragma once

#include "QxDockGlobal.h"
#include <QPushButton>

QX_BEGIN_NAMESPACE


/**
 * QxDock specific push button class with orientation support
 */
class QX_DOCK_EXPORT DockPushButton : public QPushButton
{
    Q_OBJECT
public:
    enum Orientation {
        Horizontal,
        VerticalTopToBottom,
        VerticalBottomToTop
    };

    using QPushButton::QPushButton;

    virtual QSize sizeHint() const override;

    /**
     * Returns the current orientation
     */
    Orientation buttonOrientation() const;

    /**
     * Set the orientation of this button
     */
    void setButtonOrientation(Orientation orientation);
protected:
    virtual void paintEvent(QPaintEvent *event) override;
private:
    Orientation m_Orientation = Horizontal;
};

QX_END_NAMESPACE
