/**
 * Copyright (C) 2023 maminjie <canpool@163.com>
 * Copyright (C) 2020 Uwe Kindler
 * SPDX-License-Identifier: LGPL-2.1
 **/
#pragma once

#include "QxDockManager.h"
#include "QxDockGlobal.h"
#include <QObject>

QX_BEGIN_NAMESPACE

struct DockFocusControllerPrivate;
class DockManager;
class DockFloatingContainer;

/**
 * Manages focus styling of dock widgets and handling of focus changes
 */
class QX_DOCK_EXPORT DockFocusController : public QObject
{
    Q_OBJECT
private:
    DockFocusControllerPrivate *d;   ///< private data (pimpl)
    friend struct DockFocusControllerPrivate;

private Q_SLOTS:
    void onApplicationFocusChanged(QWidget *old, QWidget *now);
    void onFocusWindowChanged(QWindow *focusWindow);
    void onFocusedDockAreaViewToggled(bool Open);
    void onStateRestored();
    void onDockWidgetVisibilityChanged(bool Visible);
public:
    using Super = QObject;
    /**
     * Default Constructor
     */
    DockFocusController(DockManager *dockManager);

    /**
     * Virtual Destructor
     */
    virtual ~DockFocusController();

    /**
     * A container needs to call this function if a widget has been dropped
     * into it
     */
    void notifyWidgetOrAreaRelocation(QWidget *RelocatedWidget);

    /**
     * This function is called, if a floating widget has been dropped into
     * an new position.
     * When this function is called, all dock widgets of the FloatingWidget
     * are already inserted into its new position
     */
    void notifyFloatingWidgetDrop(DockFloatingContainer *FloatingWidget);

    /**
     * Returns the dock widget that has focus style in the ui or a nullptr if
     * not dock widget is painted focused.
     */
    DockWidget *focusedDockWidget() const;

    /**
     * Request focus highlighting for the given dock widget assigned to the tab
     * given in Tab parameter
     */
    void setDockWidgetTabFocused(DockWidgetTab *Tab);

    /*
     * Request clear focus for a dock widget
     */
    void clearDockWidgetFocus(DockWidget *dockWidget);

public Q_SLOTS:
    /**
     * Request a focus change to the given dock widget
     */
    void setDockWidgetFocused(DockWidget *focusedNow);
};   // class DockFocusController

QX_END_NAMESPACE
