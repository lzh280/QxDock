/**
 * Copyright (C) 2023 maminjie <canpool@163.com>
 * Copyright (C) 2017-2018 Uwe Kindler
 * SPDX-License-Identifier: LGPL-2.1
 **/
#pragma once

#include "QxDockGlobal.h"
#include <QFrame>

QT_FORWARD_DECLARE_CLASS(QAbstractButton)

QX_BEGIN_NAMESPACE

class DockAreaTabBar;
class DockAreaWidget;
struct DockAreaTitleBarPrivate;
class DockElidingLabel;

/**
 * Title bar of a dock area.
 * The title bar contains a tabbar with all tabs for a dock widget group and
 * with a tabs menu button, a undock button and a close button.
 */
class QX_DOCK_EXPORT DockAreaTitleBar : public QFrame
{
    Q_OBJECT
private:
    DockAreaTitleBarPrivate *d;   ///< private data (pimpl)
    friend struct DockAreaTitleBarPrivate;

private Q_SLOTS:
    void onTabsMenuAboutToShow();
    void onCloseButtonClicked();
    void onUndockButtonClicked();
    void onTabsMenuActionTriggered(QAction *Action);
    void onCurrentTabChanged(int Index);
    void onAutoHideButtonClicked();
    void onAutoHideDockAreaActionClicked();
    void onAutoHideToActionClicked();
protected:
    /**
     * Stores mouse position to detect dragging
     */
    virtual void mousePressEvent(QMouseEvent *ev) override;

    /**
     * Stores mouse position to detect dragging
     */
    virtual void mouseReleaseEvent(QMouseEvent *ev) override;

    /**
     * Starts floating the complete docking area including all dock widgets,
     * if it is not the last dock area in a floating widget
     */
    virtual void mouseMoveEvent(QMouseEvent *ev) override;

    /**
     * Double clicking the title bar also starts floating of the complete area
     */
    virtual void mouseDoubleClickEvent(QMouseEvent *event) override;

    /**
     * Show context menu
     */
    virtual void contextMenuEvent(QContextMenuEvent *event) override;

public Q_SLOTS:
    /**
     * Call this slot to tell the title bar that it should update the tabs menu
     * the next time it is shown.
     */
    void markTabsMenuOutdated();
public:
    using Super = QFrame;

    /**
     * Default Constructor
     */
    DockAreaTitleBar(DockAreaWidget *parent);

    /**
     * Virtual Destructor
     */
    virtual ~DockAreaTitleBar();

    /**
     * Returns the pointer to the tabBar()
     */
    DockAreaTabBar *tabBar() const;

    /**
     * Returns the button corresponding to the given title bar button identifier
     */
    QAbstractButton *button(TitleBarButton which) const;

    /**
     * Returns the auto hide title label, used when the dock area is expanded and auto hidden
     */
    DockElidingLabel *autoHideTitleLabel() const;

    /**
     * Updates the visibility of the dock widget actions in the title bar
     */
    void updateDockWidgetActionsButtons();

    /**
     * Marks the tabs menu outdated before it calls its base class
     * implementation
     */
    virtual void setVisible(bool Visible) override;

    /**
     * Inserts a custom widget at position index into this title bar.
     * If index is negative, the widget is added at the end.
     * You can use this function to insert custom widgets into the title bar.
     */
    void insertWidget(int index, QWidget *widget);

    /**
     * Searches for widget widget in this title bar.
     * You can use this function, to get the position of the default
     * widget in the tile bar.
     * \code
     * int tabBarIndex = TitleBar->indexOf(TitleBar->tabBar());
     * int closeButtonIndex = TitleBar->indexOf(TitleBar->button(TitleBarButtonClose));
     * \endcode
     */
    int indexOf(QWidget *widget) const;

    /**
     * Close group tool tip based on the current state
     * Auto hide widgets can only have one dock widget so it does not make sense for the tooltip to show close group
     */
    QString titleBarButtonToolTip(TitleBarButton Button) const;

Q_SIGNALS:
    /**
     * This signal is emitted if a tab in the tab bar is clicked by the user
     * or if the user clicks on a tab item in the title bar tab menu.
     */
    void tabBarClicked(int index);
};   // class name

QX_END_NAMESPACE

