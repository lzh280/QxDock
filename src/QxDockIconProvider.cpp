/**
 * Copyright (C) 2023 maminjie <canpool@163.com>
 * Copyright (C) 2017-2019 Uwe Kindler
 * SPDX-License-Identifier: LGPL-2.1
 **/

#include "QxDockIconProvider.h"

#include <QVector>

QX_BEGIN_NAMESPACE

/**
 * Private data class (pimpl)
 */
struct DockIconProviderPrivate {
    DockIconProvider *_this;
    QVector<QIcon> UserIcons{IconCount, QIcon()};

    /**
     * Private data constructor
     */
    DockIconProviderPrivate(DockIconProvider *_public);
};
// struct LedArrayPanelPrivate

DockIconProviderPrivate::DockIconProviderPrivate(DockIconProvider *_public) : _this(_public)
{
}

DockIconProvider::DockIconProvider() : d(new DockIconProviderPrivate(this))
{
}

DockIconProvider::~DockIconProvider()
{
    delete d;
}

QIcon DockIconProvider::customIcon(eIcon IconId) const
{
    Q_ASSERT(IconId < d->UserIcons.size());
    return d->UserIcons[IconId];
}

void DockIconProvider::registerCustomIcon(eIcon IconId, const QIcon &icon)
{
    Q_ASSERT(IconId < d->UserIcons.size());
    d->UserIcons[IconId] = icon;
}

QX_END_NAMESPACE
