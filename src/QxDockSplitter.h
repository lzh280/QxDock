/**
 * Copyright (C) 2023 maminjie <canpool@163.com>
 * Copyright (C) 2017 Uwe Kindler
 * SPDX-License-Identifier: LGPL-2.1
 **/
#pragma once

#include "QxDockGlobal.h"
#include <QSplitter>

QX_BEGIN_NAMESPACE

struct DockSplitterPrivate;

/**
 * Splitter used internally instead of QSplitter with some additional
 * fuctionality.
 */
class QX_DOCK_EXPORT DockSplitter : public QSplitter
{
    Q_OBJECT
private:
    DockSplitterPrivate *d;
    friend struct DockSplitterPrivate;
public:
    DockSplitter(QWidget *parent = Q_NULLPTR);
    DockSplitter(Qt::Orientation orientation, QWidget *parent = Q_NULLPTR);

    /**
     * Prints debug info
     */
    virtual ~DockSplitter();

    /**
     * Returns true, if any of the internal widgets is visible
     */
    bool hasVisibleContent() const;

    /**
     * Returns first widget or nullptr if splitter is empty
     */
    QWidget *firstWidget() const;

    /**
     * Returns last widget of nullptr is splitter is empty
     */
    QWidget *lastWidget() const;

    /**
     * Returns true if the splitter contains central widget of dock manager.
     */
    bool isResizingWithContainer() const;
};   // class DockSplitter

QX_END_NAMESPACE

