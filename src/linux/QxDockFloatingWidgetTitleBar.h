/**
 * Copyright (C) 2023 maminjie <canpool@163.com>
 * Copyright (C) 2017-2019 Uwe Kindler
 * SPDX-License-Identifier: LGPL-2.1
 **/
#pragma once

#include "QxDockGlobal.h"
#include <QFrame>
#include <QIcon>

QX_BEGIN_NAMESPACE

class DockFloatingContainer;

struct DockFloatingWidgetTitleBarPrivate;

/**
 * Titlebar for floating widgets to capture non client are mouse events.
 * Linux does not support NonClieantArea mouse events like
 * QEvent::NonClientAreaMouseButtonPress. Because these events are required
 * for the docking system to work properly, we use our own titlebar here to
 * capture the required mouse events.
 */
class DockFloatingWidgetTitleBar : public QFrame
{
    Q_OBJECT
    Q_PROPERTY(QIcon maximizeIcon READ maximizeIcon WRITE setMaximizeIcon)
    Q_PROPERTY(QIcon normalIcon READ normalIcon WRITE setNormalIcon)
private:
    DockFloatingWidgetTitleBarPrivate *d;   ///< private data (pimpl)
protected:
    virtual void mousePressEvent(QMouseEvent *ev) override;
    virtual void mouseReleaseEvent(QMouseEvent *ev) override;
    virtual void mouseMoveEvent(QMouseEvent *ev) override;
    virtual void mouseDoubleClickEvent(QMouseEvent *event) override;

    void setMaximizeIcon(const QIcon &Icon);
    QIcon maximizeIcon() const;
    void setNormalIcon(const QIcon &Icon);
    QIcon normalIcon() const;
public:
    using Super = QWidget;
    explicit DockFloatingWidgetTitleBar(DockFloatingContainer *parent = nullptr);

    /**
     * Virtual Destructor
     */
    virtual ~DockFloatingWidgetTitleBar();

    /**
     * Enables / disables the window close button.
     */
    void enableCloseButton(bool Enable);

    /**
     * Sets the window title, that means, the text of the internal tile label.
     */
    void setTitle(const QString &Text);

    /**
     * Update stylesheet style if a property changes
     */
    void updateStyle();

    /**
     * Change the maximize button icon according to current windows state
     */
    void setMaximizedIcon(bool maximized);

signals:
    /**
     * This signal is emitted, if the close button is clicked.
     */
    void closeRequested();

    /**
     * This signal is emitted, if the maximize button is clicked.
     */
    void maximizeRequested();
};

QX_END_NAMESPACE
