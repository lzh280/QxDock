/**
 * Copyright (C) 2023 maminjie <canpool@163.com>
 * Copyright (C) 2022 Uwe Kindler
 * SPDX-License-Identifier: LGPL-2.1
 **/

#include "QxDockPushButton.h"

#include <QDebug>
#include <QPainter>
#include <QStyleOptionButton>
#include <QStylePainter>

QX_BEGIN_NAMESPACE

QSize DockPushButton::sizeHint() const
{
    QSize sh = QPushButton::sizeHint();

    if (m_Orientation != DockPushButton::Horizontal) {
        sh.transpose();
    }

    return sh;
}

void DockPushButton::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event);

    QStylePainter painter(this);
    QStyleOptionButton option;
    initStyleOption(&option);

    if (m_Orientation == DockPushButton::VerticalTopToBottom) {
        painter.rotate(90);
        painter.translate(0, -1 * width());
        option.rect = option.rect.transposed();
    } else if (m_Orientation == DockPushButton::VerticalBottomToTop) {
        painter.rotate(-90);
        painter.translate(-1 * height(), 0);
        option.rect = option.rect.transposed();
    }

    painter.drawControl(QStyle::CE_PushButton, option);
}

DockPushButton::Orientation DockPushButton::buttonOrientation() const
{
    return m_Orientation;
}

void DockPushButton::setButtonOrientation(Orientation orientation)
{
    m_Orientation = orientation;
    updateGeometry();
}

QX_END_NAMESPACE
