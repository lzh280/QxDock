/**
 * Copyright (C) 2023 maminjie <canpool@163.com>
 * Copyright (C) 2020 Uwe Kindler
 * SPDX-License-Identifier: LGPL-2.1
 **/

#include "QxDockFactory.h"
#include "QxDockAreaTabBar.h"
#include "QxDockAreaTitleBar.h"
#include "QxDockAreaWidget.h"
#include "QxDockWidget.h"
#include "QxDockWidgetTab.h"
#include "QxDockAutoHideTab.h"

#include <memory>

QX_BEGIN_NAMESPACE

static std::unique_ptr<DockComponentsFactory> DefaultFactory(new DockComponentsFactory());

DockWidgetTab *DockComponentsFactory::createDockWidgetTab(DockWidget *dockWidget) const
{
    return new DockWidgetTab(dockWidget);
}

DockAutoHideTab *DockComponentsFactory::createDockWidgetSideTab(DockWidget *dockWidget) const
{
    return new DockAutoHideTab(dockWidget);
}

DockAreaTabBar *DockComponentsFactory::createDockAreaTabBar(DockAreaWidget *dockArea) const
{
    return new DockAreaTabBar(dockArea);
}

DockAreaTitleBar *DockComponentsFactory::createDockAreaTitleBar(DockAreaWidget *dockArea) const
{
    return new DockAreaTitleBar(dockArea);
}

const DockComponentsFactory *DockComponentsFactory::factory()
{
    return DefaultFactory.get();
}

void DockComponentsFactory::setFactory(DockComponentsFactory *Factory)
{
    DefaultFactory.reset(Factory);
}

void DockComponentsFactory::resetDefaultFactory()
{
    DefaultFactory.reset(new DockComponentsFactory());
}

QX_END_NAMESPACE
