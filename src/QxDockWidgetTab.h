/**
 * Copyright (C) 2023 maminjie <canpool@163.com>
 * Copyright (C) 2017 Uwe Kindler
 * SPDX-License-Identifier: LGPL-2.1
 **/
#pragma once

#include "QxDockGlobal.h"
#include <QFrame>
#include <QSize>

QX_BEGIN_NAMESPACE

class DockWidget;
class DockAreaWidget;
struct DockWidgetTabPrivate;
class DockManager;

/**
 * A dock widget tab that shows a title and an icon.
 * The dock widget tab is shown in the dock area title bar to switch between
 * tabbed dock widgets
 */
class QX_DOCK_EXPORT DockWidgetTab : public QFrame
{
    Q_OBJECT
    Q_PROPERTY(bool activeTab READ isActiveTab WRITE setActiveTab NOTIFY activeTabChanged)
    Q_PROPERTY(QSize iconSize READ iconSize WRITE setIconSize)
private:
    DockWidgetTabPrivate *d;   ///< private data (pimpl)
    friend struct DockWidgetTabPrivate;
    friend class DockWidget;
    friend class DockManager;
    friend class DockAutoHideContainer;
    void onDockWidgetFeaturesChanged();

private Q_SLOTS:
    void detachDockWidget();
    void autoHideDockWidget();
    void onAutoHideToActionClicked();
protected:
    virtual void mousePressEvent(QMouseEvent *ev) override;
    virtual void mouseReleaseEvent(QMouseEvent *ev) override;
    virtual void mouseMoveEvent(QMouseEvent *ev) override;
    virtual void contextMenuEvent(QContextMenuEvent *ev) override;

    /**
     * Double clicking the tab widget makes the assigned dock widget floating
     */
    virtual void mouseDoubleClickEvent(QMouseEvent *event) override;
public:
    using Super = QFrame;
    /**
     * Default Constructor
     * param[in] dockWidget The dock widget this title bar belongs to
     * param[in] parent The parent widget of this title bar
     */
    DockWidgetTab(DockWidget *dockWidget, QWidget *parent = 0);

    /**
     * Virtual Destructor
     */
    virtual ~DockWidgetTab();

    /**
     * Returns true, if this is the active tab
     */
    bool isActiveTab() const;

    /**
     * Set this true to make this tab the active tab
     */
    void setActiveTab(bool active);

    /**
     * Sets the dock area widget the dockWidget returned by dockWidget()
     * function belongs to.
     */
    void setDockAreaWidget(DockAreaWidget *dockArea);

    /**
     * Returns the dock area widget this title bar belongs to.
     * \return This function returns 0 if the dock widget that owns this title
     * bar widget has not been added to any dock area yet.
     */
    DockAreaWidget *dockAreaWidget() const;

    /**
     * Returns the dock widget this title widget belongs to
     */
    DockWidget *dockWidget() const;

    /**
     * Sets the icon to show in title bar
     */
    void setIcon(const QIcon &Icon);

    /**
     * Returns the icon
     */
    const QIcon &icon() const;

    /**
     * Returns the tab text
     */
    QString text() const;

    /**
     * Sets the tab text
     */
    void setText(const QString &title);

    /**
     * Returns true if text is elided on the tab's title
     */
    bool isTitleElided() const;

    /**
     * This function returns true if the assigned dock widget is closable
     */
    bool isClosable() const;

    /**
     * Track event ToolTipChange and set child ToolTip
     */
    virtual bool event(QEvent *e) override;

    /**
     * Sets the text elide mode
     */
    void setElideMode(Qt::TextElideMode mode);

    /**
     * Update stylesheet style if a property changes
     */
    void updateStyle();

    /**
     * Returns the icon size.
     * If no explicit icon size has been set, the function returns an invalid
     * QSize
     */
    QSize iconSize() const;

    /**
     * Set an explicit icon size.
     * If no icon size has been set explicitely, than the tab sets the icon size
     * depending on the style
     */
    void setIconSize(const QSize &Size);

public Q_SLOTS:
    virtual void setVisible(bool visible) override;

Q_SIGNALS:
    void activeTabChanged();
    void clicked();
    void closeRequested();
    void closeOtherTabsRequested();
    void moved(const QPoint &GlobalPos);
    void elidedChanged(bool elided);
};   // class DockWidgetTab

QX_END_NAMESPACE
