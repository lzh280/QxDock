/**
 * Copyright (C) 2023 maminjie <canpool@163.com>
 * Copyright (C) 2022 Syarif Fakhri
 * Copyright (C) 2017 Uwe Kindler
 * SPDX-License-Identifier: LGPL-2.1
 **/
#pragma once

#include "QxDockAutoHideTab.h"
#include "QxDockGlobal.h"
#include <QSplitter>

class QXmlStreamWriter;

QX_BEGIN_NAMESPACE

struct DockAutoHideContainerPrivate;
class DockManager;
class DockWidget;
class DockContainerWidget;
class DockAutoHideSideBar;
class DockAreaWidget;
class DockStateReader;
struct DockSideTabBarPrivate;

/**
 * Auto hide container for hosting an auto hide dock widget
 */
class QX_DOCK_EXPORT DockAutoHideContainer : public QFrame
{
    Q_OBJECT
    Q_PROPERTY(int sideBarLocation READ sideBarLocation)
private:
    DockAutoHideContainerPrivate *d;   ///< private data (pimpl)
    friend struct DockAutoHideContainerPrivate;
    friend DockAutoHideSideBar;
    friend DockSideTabBarPrivate;
protected:
    virtual bool eventFilter(QObject *watched, QEvent *event) override;
    virtual void resizeEvent(QResizeEvent *event) override;
    virtual void leaveEvent(QEvent *event) override;
    virtual bool event(QEvent *event) override;

    /**
     * Updates the size considering the size limits and the resize margins
     */
    void updateSize();

    /*
     * Saves the state and size
     */
    void saveState(QXmlStreamWriter &Stream);
public:
    using Super = QFrame;

    /**
     * Create Auto Hide widget with the given dock widget
     */
    DockAutoHideContainer(DockWidget *dockWidget, SideBarLocation area, DockContainerWidget *parent);

    /**
     * Virtual Destructor
     */
    virtual ~DockAutoHideContainer();

    /**
     * Get's the side tab bar
     */
    DockAutoHideSideBar *sideBar() const;

    /**
     * Returns the side tab
     */
    DockAutoHideTab *autoHideTab() const;

    /**
     * Get's the dock widget in this dock container
     */
    DockWidget *dockWidget() const;

    /**
     * Adds a dock widget and removes the previous dock widget
     */
    void addDockWidget(DockWidget *dockWidget);

    /**
     * Returns the side tab bar area of this Auto Hide dock container
     */
    SideBarLocation sideBarLocation() const;

    /**
     * Sets a new SideBarLocation.
     * If a new side bar location is set, the auto hide dock container needs
     * to update its resize handle position
     */
    void setSideBarLocation(SideBarLocation SideBarLocation);

    /**
     * Returns the dock area widget of this Auto Hide dock container
     */
    DockAreaWidget *dockAreaWidget() const;

    /**
     * Returns the parent container that hosts this auto hide container
     */
    DockContainerWidget *dockContainer() const;

    /**
     * Moves the contents to the parent container widget
     * Used before removing this Auto Hide dock container
     */
    void moveContentsToParent();

    /**
     * Cleanups up the side tab widget and then deletes itself
     */
    void cleanupAndDelete();

    /*
     * Toggles the auto Hide dock container widget
     * This will also hide the side tab widget
     */
    void toggleView(bool Enable);

    /*
     * Collapses the auto hide dock container widget
     * Does not hide the side tab widget
     */
    void collapseView(bool Enable);

    /**
     * Toggles the current collapse state
     */
    void toggleCollapseState();

    /**
     * Use this instead of resize.
     * Depending on the sidebar location this will set the width or heigth
     * of this auto hide container.
     */
    void setSize(int Size);
};

QX_END_NAMESPACE
