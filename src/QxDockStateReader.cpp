/**
 * Copyright (C) 2023 maminjie <canpool@163.com>
 * Copyright (C) 2019 Uwe Kindler
 * SPDX-License-Identifier: LGPL-2.1
 **/

#include "QxDockStateReader.h"

QX_BEGIN_NAMESPACE

void DockStateReader::setFileVersion(int FileVersion)
{
    m_FileVersion = FileVersion;
}

int DockStateReader::fileVersion() const
{
    return m_FileVersion;
}

QX_END_NAMESPACE
