/**
 * Copyright (C) 2023 maminjie <canpool@163.com>
 * Copyright (C) 2017-2019 Uwe Kindler
 * SPDX-License-Identifier: LGPL-2.1
 **/
#pragma once

#include "QxDockFloatingContainer.h"
#include <QWidget>

QX_BEGIN_NAMESPACE

class DockWidget;
class DockAreaWidget;
struct DockFloatingDragPreviewPrivate;

/**
 * A floating overlay is a temporary floating widget that is just used to
 * indicate the floating widget movement.
 * This widget is used as a placeholder for drag operations for non-opaque
 * docking
 */
class DockFloatingDragPreview : public QWidget, public IFloatingWidget
{
    Q_OBJECT
private:
    DockFloatingDragPreviewPrivate *d;
    friend struct DockFloatingDragPreviewPrivate;

private Q_SLOTS:
    /**
     * Cancel non opaque undocking if application becomes inactive
     */
    void onApplicationStateChanged(Qt::ApplicationState state);
protected:
    /**
     * Cares about painting the
     */
    virtual void paintEvent(QPaintEvent *e) override;

    /**
     * The content is a dockArea or a dockWidget
     */
    DockFloatingDragPreview(QWidget *Content, QWidget *parent);
public:
    using Super = QWidget;

    /**
     * Creates an instance for undocking the dockWidget in Content parameter
     */
    DockFloatingDragPreview(DockWidget *Content);

    /**
     * Creates an instance for undocking the dockArea given in Content
     * parameters
     */
    DockFloatingDragPreview(DockAreaWidget *Content);

    /**
     * Delete private data
     */
    ~DockFloatingDragPreview();

    /**
     * We filter the events of the assigned content widget to receive
     * escape key presses for canceling the drag operation
     */
    virtual bool eventFilter(QObject *watched, QEvent *event) override;
public:   // implements IFloatingWidget -----------------------------------------
    virtual void startFloating(const QPoint &DragStartMousePos, const QSize &Size, eDragState DragState,
                               QWidget *MouseEventHandler) override;

    /**
     * Moves the widget to a new position relative to the position given when
     * startFloating() was called
     */
    virtual void moveFloating() override;

    /**
     * Finishes dragging.
     * Hides the dock overlays and executes the real undocking and docking
     * of the assigned Content widget
     */
    virtual void finishDragging() override;

    /**
     * Cleanup auto hide container if the dragged widget has one
     */
    void cleanupAutoHideContainerWidget();

Q_SIGNALS:
    /**
     * This signal is emitted, if dragging has been canceled by escape key
     * or by active application switching via task manager
     */
    void draggingCanceled();
};

QX_END_NAMESPACE
