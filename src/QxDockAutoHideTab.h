/**
 * Copyright (C) 2023 maminjie <canpool@163.com>
 * Copyright (C) 2022 Syarif Fakhri
 * Copyright (C) 2017 Uwe Kindler
 * SPDX-License-Identifier: LGPL-2.1
 **/
#pragma once

#include "QxDockPushButton.h"
#include "QxDockGlobal.h"

QX_BEGIN_NAMESPACE

struct DockAutoHideTabPrivate;
class DockWidget;
class DockAutoHideSideBar;
class DockWidgetTab;
class DockContainerWidgetPrivate;

/**
 * A dock widget Side tab that shows a title or an icon.
 * The dock widget tab is shown in the side tab bar to switch between
 * pinned dock widgets
 */
class QX_DOCK_EXPORT DockAutoHideTab : public DockPushButton
{
    Q_OBJECT

    Q_PROPERTY(int sideBarLocation READ sideBarLocation)
    Q_PROPERTY(Qt::Orientation orientation READ orientation)
    Q_PROPERTY(bool activeTab READ isActiveTab)
    Q_PROPERTY(bool iconOnly READ iconOnly)
private:
    DockAutoHideTabPrivate *d;   ///< private data (pimpl)
    friend struct DockAutoHideTabPrivate;
    friend class DockWidget;
    friend class DockAutoHideContainer;
    friend class DockAutoHideSideBar;
    friend class DockAreaWidget;
    friend class DockContainerWidget;
    friend DockContainerWidgetPrivate;
protected:
    void setSideBar(DockAutoHideSideBar *SideTabBar);
    void removeFromSideBar();
    virtual bool event(QEvent *event) override;
public:
    using Super = DockPushButton;

    /**
     * Default Constructor
     * param[in] dockWidget The dock widget this title bar belongs to
     * param[in] parent The parent widget of this title bar
     */
    DockAutoHideTab(QWidget *parent = nullptr);

    /**
     * Virtual Destructor
     */
    virtual ~DockAutoHideTab();

    /**
     * Update stylesheet style if a property changes
     */
    void updateStyle();

    /**
     * Getter for side tab bar area property
     */
    SideBarLocation sideBarLocation() const;

    /**
     * Set orientation vertical or horizontal
     */
    void setOrientation(Qt::Orientation Orientation);

    /**
     * Returns the current orientation
     */
    Qt::Orientation orientation() const;

    /**
     * Returns true, if this is the active tab. The tab is active if the auto
     * hide widget is visible
     */
    bool isActiveTab() const;

    /**
     * returns the dock widget this belongs to
     */
    DockWidget *dockWidget() const;

    /**
     * Sets the dock widget that is controlled by this tab
     */
    void setDockWidget(DockWidget *dockWidget);

    /**
     * Returns true if the auto hide config flag AutoHideSideBarsIconOnly
     * is set and if the tab has an icon - that means the icon is not null
     */
    bool iconOnly() const;

    /**
     * Returns the side bar that contains this tab or a nullptr if the tab is
     * not in a side bar
     */
    DockAutoHideSideBar *sideBar() const;
};   // class AutoHideTab

QX_END_NAMESPACE
