/**
 * Copyright (C) 2023 maminjie <canpool@163.com>
 * Copyright (C) 2017-2018 Uwe Kindler
 * SPDX-License-Identifier: LGPL-2.1
 **/
#pragma once

#include "QxDockGlobal.h"
#include <QFrame>
#include <QToolButton>

QX_BEGIN_NAMESPACE

using tTitleBarButton = QToolButton;

/**
 * Title bar button of a dock area that customizes tTitleBarButton appearance/behaviour
 * according to various config flags such as:
 * DockManager::DockAreaHas_xxx_Button - if set to 'false' keeps the button always invisible
 * DockManager::DockAreaHideDisabledButtons - if set to 'true' hides button when it is disabled
 */
class DockTitleBarButton : public tTitleBarButton
{
    Q_OBJECT
private:
    bool Visible = true;
    bool HideWhenDisabled = false;
public:
    using Super = tTitleBarButton;
    DockTitleBarButton(bool visible = true, QWidget *parent = nullptr);

    /**
     * Adjust this visibility change request with our internal settings:
     */
    virtual void setVisible(bool visible) override;
protected:
    /**
     * Handle EnabledChanged signal to set button invisible if the configured
     */
    bool event(QEvent *ev) override;
};

/**
 * This spacer widget is here because of the following problem.
 * The dock area title bar handles mouse dragging and moving the floating widget.
 * The  problem is, that if the title bar becomes invisible, i.e. if the dock
 * area contains only one single dock widget and the dock area is moved
 * into a floating widget, then mouse events are not handled anymore and dragging
 * of the floating widget stops.
 */
class DockSpacerWidget : public QWidget
{
    Q_OBJECT
public:
    using Super = QWidget;
    DockSpacerWidget(QWidget *Parent = 0);
    virtual QSize sizeHint() const override
    {
        return QSize(0, 0);
    }
    virtual QSize minimumSizeHint() const override
    {
        return QSize(0, 0);
    }
};

QX_END_NAMESPACE

