QT += core gui widgets

OUT_ROOT = $${OUT_PWD}/..
TEMPLATE = lib
CONFIG += c++14
CONFIG += debug_and_release
TARGET = $$qtLibraryTarget(QxDock)
DESTDIR = $${OUT_ROOT}/bin

!QxDockBuildStatic {
    CONFIG += shared
    DEFINES += QX_DOCK_SHARED_EXPORT
}
QxDockBuildStatic {
    CONFIG += staticlib
    DEFINES += QX_DOCK_STATIC
}

include($$PWD/QxDock.pri)

isEmpty(PREFIX){
    PREFIX=../installed
    warning("Install Prefix not set")
}
headers.path=$$PREFIX/include
headers.files=$$HEADERS
target.path=$$PREFIX/lib
INSTALLS += headers target

DISTFILES +=
