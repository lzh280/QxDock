/**
 * Copyright (C) 2023 maminjie <canpool@163.com>
 * Copyright (C) 2017 Uwe Kindler
 * SPDX-License-Identifier: LGPL-2.1
 **/

#include "QxDockWidget.h"
#include "QxDockAreaWidget.h"
#include "QxDockFactory.h"
#include "QxDockContainerWidget.h"
#include "QxDockManager.h"
#include "QxDockSplitter.h"
#include "QxDockWidgetTab.h"
#include "QxDockFloatingContainer.h"
#include "QxDockAutoHideContainer.h"
#include "QxDockAutoHideSideBar.h"
#include "QxDockAutoHideTab.h"

#include <QAction>
#include <QBoxLayout>
#include <QDebug>
#include <QEvent>
#include <QGuiApplication>
#include <QPointer>
#include <QScreen>
#include <QScrollArea>
#include <QSplitter>
#include <QStack>
#include <QTextStream>
#include <QToolBar>
#include <QWindow>
#include <QXmlStreamWriter>
#include <iostream>

QX_BEGIN_NAMESPACE

/**
 * Private data class of DockWidget class (pimpl)
 */
struct DockWidgetPrivate {
    struct WidgetFactory {
        DockWidget::FactoryFunc createWidget;
        DockWidget::eInsertMode insertMode;
    };

    DockWidget *_this = nullptr;
    QBoxLayout *Layout = nullptr;
    QWidget *Widget = nullptr;
    DockWidgetTab *TabWidget = nullptr;
    DockWidget::DockWidgetFeatures Features = DockWidget::DefaultDockWidgetFeatures;
    DockManager *dockManager = nullptr;
    DockAreaWidget *dockArea = nullptr;
    QAction *ToggleViewAction = nullptr;
    bool Closed = false;
    QScrollArea *ScrollArea = nullptr;
    QToolBar *ToolBar = nullptr;
    Qt::ToolButtonStyle ToolBarStyleDocked = Qt::ToolButtonIconOnly;
    Qt::ToolButtonStyle ToolBarStyleFloating = Qt::ToolButtonTextUnderIcon;
    QSize ToolBarIconSizeDocked = QSize(16, 16);
    QSize ToolBarIconSizeFloating = QSize(24, 24);
    bool IsFloatingTopLevel = false;
    QList<QAction *> TitleBarActions;
    DockWidget::eMinimumSizeHintMode MinimumSizeHintMode = DockWidget::MinimumSizeHintFromDockWidget;
    WidgetFactory *Factory = nullptr;
    QPointer<DockAutoHideTab> SideTabWidget;

    /**
     * Private data constructor
     */
    DockWidgetPrivate(DockWidget *_public);

    /**
     * Show dock widget
     */
    void showDockWidget();

    /**
     * Hide dock widget.
     */
    void hideDockWidget();

    /**
     * Hides a dock area if all dock widgets in the area are closed.
     * This function updates the current selected tab and hides the parent
     * dock area if it is empty
     */
    void updateParentDockArea();

    /**
     * Closes all auto hide dock widgets if there are no more opened dock areas
     * This prevents the auto hide dock widgets from being pinned to an empty dock area
     */
    void closeAutoHideDockWidgetsIfNeeded();

    /**
     * Setup the top tool bar
     */
    void setupToolBar();

    /**
     * Setup the main scroll area
     */
    void setupScrollArea();

    /**
     * Creates the content widget with the registered widget factory and
     * returns true on success.
     */
    bool createWidgetFromFactory();
};
// struct DockWidgetPrivate

DockWidgetPrivate::DockWidgetPrivate(DockWidget *_public) : _this(_public)
{
}

void DockWidgetPrivate::showDockWidget()
{
    if (!Widget) {
        if (!createWidgetFromFactory()) {
            Q_ASSERT(!Features.testFlag(DockWidget::DeleteContentOnClose) &&
                     "DeleteContentOnClose flag was set, but the widget "
                     "factory is missing or it doesn't return a valid QWidget.");
            return;
        }
    }

    if (!dockArea) {
        DockFloatingContainer *FloatingWidget = new DockFloatingContainer(_this);
        // We use the size hint of the content widget to provide a good
        // initial size
        FloatingWidget->resize(Widget ? Widget->sizeHint() : _this->sizeHint());
        TabWidget->show();
        FloatingWidget->show();
    } else {
        dockArea->setCurrentDockWidget(_this);
        dockArea->toggleView(true);
        TabWidget->show();
        QSplitter *Splitter = internal::findParent<QSplitter *>(dockArea);
        while (Splitter && !Splitter->isVisible() && !dockArea->isAutoHide()) {
            Splitter->show();
            Splitter = internal::findParent<QSplitter *>(Splitter);
        }

        DockContainerWidget *Container = dockArea->dockContainer();
        if (Container->isFloating()) {
            DockFloatingContainer *FloatingWidget = internal::findParent<DockFloatingContainer *>(Container);
            FloatingWidget->show();
        }

        // If this widget is pinned and there are no opened dock widgets, unpin the auto hide widget by moving it's
        // contents to parent container While restoring state, opened dock widgets are not valid
        if (Container->openedDockWidgets().count() == 0 && dockArea->isAutoHide() && !dockManager->isRestoringState()) {
            dockArea->autoHideDockContainer()->moveContentsToParent();
        }
    }
}

void DockWidgetPrivate::hideDockWidget()
{
    TabWidget->hide();
    updateParentDockArea();

    closeAutoHideDockWidgetsIfNeeded();

    if (Features.testFlag(DockWidget::DeleteContentOnClose)) {
        Widget->deleteLater();
        Widget = nullptr;
    }
}

void DockWidgetPrivate::updateParentDockArea()
{
    if (!dockArea) {
        return;
    }

    // we don't need to change the current tab if the
    // current dock widget is not the one being closed
    if (dockArea->currentDockWidget() != _this) {
        return;
    }

    auto NextDockWidget = dockArea->nextOpenDockWidget(_this);
    if (NextDockWidget) {
        dockArea->setCurrentDockWidget(NextDockWidget);
    } else {
        dockArea->hideAreaWithNoVisibleContent();
    }
}

void DockWidgetPrivate::closeAutoHideDockWidgetsIfNeeded()
{
    auto DockContainer = _this->dockContainer();
    if (!DockContainer) {
        return;
    }

    if (_this->dockManager()->isRestoringState()) {
        return;
    }

    if (!DockContainer->openedDockWidgets().isEmpty()) {
        return;
    }

    for (auto autoHideWidget : DockContainer->autoHideWidgets()) {
        auto dockWidget = autoHideWidget->dockWidget();
        if (dockWidget == _this) {
            continue;
        }

        dockWidget->toggleView(false);
    }
}

void DockWidgetPrivate::setupToolBar()
{
    ToolBar = new QToolBar(_this);
    ToolBar->setObjectName("dockWidgetToolBar");
    Layout->insertWidget(0, ToolBar);
    ToolBar->setIconSize(QSize(16, 16));
    ToolBar->toggleViewAction()->setEnabled(false);
    ToolBar->toggleViewAction()->setVisible(false);
    _this->connect(_this, SIGNAL(topLevelChanged(bool)), SLOT(setToolbarFloatingStyle(bool)));
}

void DockWidgetPrivate::setupScrollArea()
{
    ScrollArea = new QScrollArea(_this);
    ScrollArea->setObjectName("dockWidgetScrollArea");
    ScrollArea->setWidgetResizable(true);
    Layout->addWidget(ScrollArea);
}

bool DockWidgetPrivate::createWidgetFromFactory()
{
    if (!Features.testFlag(DockWidget::DeleteContentOnClose)) {
        return false;
    }

    if (!Factory) {
        return false;
    }

    QWidget *w = Factory->createWidget(_this);
    if (!w) {
        return false;
    }

    _this->setWidget(w, Factory->insertMode);
    return true;
}

DockWidget::DockWidget(const QString &title, QWidget *parent) : QFrame(parent), d(new DockWidgetPrivate(this))
{
    d->Layout = new QBoxLayout(QBoxLayout::TopToBottom);
    d->Layout->setContentsMargins(0, 0, 0, 0);
    d->Layout->setSpacing(0);
    setLayout(d->Layout);
    setWindowTitle(title);
    setObjectName(title);

    d->TabWidget = componentsFactory()->createDockWidgetTab(this);

    d->ToggleViewAction = new QAction(title, this);
    d->ToggleViewAction->setCheckable(true);
    connect(d->ToggleViewAction, SIGNAL(triggered(bool)), this, SLOT(toggleView(bool)));
    setToolbarFloatingStyle(false);

    if (DockManager::testConfigFlag(DockManager::FocusHighlighting)) {
        setFocusPolicy(Qt::ClickFocus);
    }
}

DockWidget::~DockWidget()
{
    QX_DOCK_PRINT("~DockWidget()");
    delete d;
}

void DockWidget::setToggleViewActionChecked(bool Checked)
{
    QAction *Action = d->ToggleViewAction;
    Action->blockSignals(true);
    Action->setChecked(Checked);
    Action->blockSignals(false);
}

void DockWidget::setWidget(QWidget *widget, eInsertMode InsertMode)
{
    if (d->Widget) {
        takeWidget();
    }

    auto ScrollAreaWidget = qobject_cast<QAbstractScrollArea *>(widget);
    if (ScrollAreaWidget || ForceNoScrollArea == InsertMode) {
        d->Layout->addWidget(widget);
        if (ScrollAreaWidget && ScrollAreaWidget->viewport()) {
            ScrollAreaWidget->viewport()->setProperty("dockWidgetContent", true);
        }
    } else {
        d->setupScrollArea();
        d->ScrollArea->setWidget(widget);
    }

    d->Widget = widget;
    d->Widget->setProperty("dockWidgetContent", true);
}

void DockWidget::setWidgetFactory(FactoryFunc createWidget, eInsertMode insertMode)
{
    if (d->Factory) {
        delete d->Factory;
    }

    d->Factory = new DockWidgetPrivate::WidgetFactory{createWidget, insertMode};
}

QWidget *DockWidget::takeWidget()
{
    QWidget *w = nullptr;
    if (d->ScrollArea) {
        d->Layout->removeWidget(d->ScrollArea);
        w = d->ScrollArea->takeWidget();
        delete d->ScrollArea;
        d->ScrollArea = nullptr;
        d->Widget = nullptr;
    } else if (d->Widget) {
        d->Layout->removeWidget(d->Widget);
        w = d->Widget;
        d->Widget = nullptr;
    }

    if (w) {
        w->setParent(nullptr);
    }
    return w;
}

QWidget *DockWidget::widget() const
{
    return d->Widget;
}

DockWidgetTab *DockWidget::tabWidget() const
{
    return d->TabWidget;
}

DockAutoHideContainer *DockWidget::autoHideDockContainer() const
{
    if (!d->dockArea) {
        return nullptr;
    }

    return d->dockArea->autoHideDockContainer();
}

void DockWidget::setFeatures(DockWidgetFeatures features)
{
    if (d->Features == features) {
        return;
    }
    d->Features = features;
    Q_EMIT featuresChanged(d->Features);
    d->TabWidget->onDockWidgetFeaturesChanged();
    if (DockAreaWidget *dockArea = dockAreaWidget())
        dockArea->onDockWidgetFeaturesChanged();
}

void DockWidget::setFeature(DockWidgetFeature flag, bool on)
{
    auto Features = features();
    internal::setFlag(Features, flag, on);
    setFeatures(Features);
}

DockWidget::DockWidgetFeatures DockWidget::features() const
{
    return d->Features;
}

DockManager *DockWidget::dockManager() const
{
    return d->dockManager;
}

void DockWidget::setDockManager(DockManager *dockManager)
{
    d->dockManager = dockManager;
}

DockContainerWidget *DockWidget::dockContainer() const
{
    if (d->dockArea) {
        return d->dockArea->dockContainer();
    } else {
        return 0;
    }
}

DockFloatingContainer *DockWidget::floatingDockContainer() const
{
    auto DockContainer = dockContainer();
    return DockContainer ? DockContainer->floatingWidget() : nullptr;
}

DockAreaWidget *DockWidget::dockAreaWidget() const
{
    return d->dockArea;
}

DockAutoHideTab *DockWidget::sideTabWidget() const
{
    return d->SideTabWidget;
}

void DockWidget::setSideTabWidget(DockAutoHideTab *SideTab) const
{
    d->SideTabWidget = SideTab;
}

bool DockWidget::isAutoHide() const
{
    return !d->SideTabWidget.isNull();
}

bool DockWidget::isFloating() const
{
    if (!isInFloatingContainer()) {
        return false;
    }

    return dockContainer()->topLevelDockWidget() == this;
}

bool DockWidget::isInFloatingContainer() const
{
    auto Container = dockContainer();
    if (!Container) {
        return false;
    }

    if (!Container->isFloating()) {
        return false;
    }

    return true;
}

bool DockWidget::isClosed() const
{
    return d->Closed;
}

QAction *DockWidget::toggleViewAction() const
{
    return d->ToggleViewAction;
}

void DockWidget::setToggleViewActionMode(eToggleViewActionMode Mode)
{
    if (ActionModeToggle == Mode) {
        d->ToggleViewAction->setCheckable(true);
        d->ToggleViewAction->setIcon(QIcon());
    } else {
        d->ToggleViewAction->setCheckable(false);
        d->ToggleViewAction->setIcon(d->TabWidget->icon());
    }
}

void DockWidget::setMinimumSizeHintMode(eMinimumSizeHintMode Mode)
{
    d->MinimumSizeHintMode = Mode;
}

bool DockWidget::isCentralWidget() const
{
    return dockManager()->centralWidget() == this;
}

void DockWidget::toggleView(bool Open)
{
    // If the toggle view action mode is ActionModeShow, then Open is always
    // true if the sender is the toggle view action
    QAction *Sender = qobject_cast<QAction *>(sender());
    if (Sender == d->ToggleViewAction && !d->ToggleViewAction->isCheckable()) {
        Open = true;
    }

    // If the dock widget state is different, then we really need to toggle
    // the state. If we are in the right state, then we simply make this
    // dock widget the current dock widget
    auto AutoHideContainer = autoHideDockContainer();
    if (d->Closed != !Open) {
        toggleViewInternal(Open);
    } else if (Open && d->dockArea && !AutoHideContainer) {
        raise();
    }

    if (Open && AutoHideContainer) {
        AutoHideContainer->collapseView(false);
    }
}

void DockWidget::toggleViewInternal(bool Open)
{
    DockContainerWidget *DockContainer = dockContainer();
    DockWidget *TopLevelDockWidgetBefore = DockContainer ? DockContainer->topLevelDockWidget() : nullptr;

    d->Closed = !Open;

    if (Open) {
        d->showDockWidget();
    } else {
        d->hideDockWidget();
    }

    d->ToggleViewAction->blockSignals(true);
    d->ToggleViewAction->setChecked(Open);
    d->ToggleViewAction->blockSignals(false);
    if (d->dockArea) {
        d->dockArea->toggleDockWidgetView(this, Open);
    }

    if (d->dockArea->isAutoHide()) {
        d->dockArea->autoHideDockContainer()->toggleView(Open);
    }

    if (Open && TopLevelDockWidgetBefore) {
        DockWidget::emitTopLevelEventForWidget(TopLevelDockWidgetBefore, false);
    }

    // Here we need to call the dockContainer() function again, because if
    // this dock widget was unassigned before the call to showDockWidget() then
    // it has a dock container now
    DockContainer = dockContainer();
    DockWidget *TopLevelDockWidgetAfter = DockContainer ? DockContainer->topLevelDockWidget() : nullptr;
    DockWidget::emitTopLevelEventForWidget(TopLevelDockWidgetAfter, true);
    DockFloatingContainer *FloatingContainer = DockContainer ? DockContainer->floatingWidget() : nullptr;
    if (FloatingContainer) {
        FloatingContainer->updateWindowTitle();
    }

    if (!Open) {
        Q_EMIT closed();
    }
    Q_EMIT viewToggled(Open);
}

void DockWidget::setDockArea(DockAreaWidget *dockArea)
{
    d->dockArea = dockArea;
    d->ToggleViewAction->setChecked(dockArea != nullptr && !this->isClosed());
    setParent(dockArea);
}

void DockWidget::saveState(QXmlStreamWriter &s) const
{
    s.writeStartElement("Widget");
    s.writeAttribute("Name", objectName());
    s.writeAttribute("Closed", QString::number(d->Closed ? 1 : 0));
    s.writeEndElement();
}

void DockWidget::flagAsUnassigned()
{
    d->Closed = true;
    setParent(d->dockManager);
    setVisible(false);
    setDockArea(nullptr);
    tabWidget()->setParent(this);
}

bool DockWidget::event(QEvent *e)
{
    switch (e->type()) {
    case QEvent::Hide:
        Q_EMIT visibilityChanged(false);
        break;

    case QEvent::Show:
        Q_EMIT visibilityChanged(geometry().right() >= 0 && geometry().bottom() >= 0);
        break;

    case QEvent::WindowTitleChange: {
        const auto title = windowTitle();
        if (d->TabWidget) {
            d->TabWidget->setText(title);
        }
        if (d->SideTabWidget) {
            d->SideTabWidget->setText(title);
        }
        if (d->ToggleViewAction) {
            d->ToggleViewAction->setText(title);
        }
        if (d->dockArea) {
            d->dockArea->markTitleBarMenuOutdated();   // update tabs menu
        }

        auto FloatingWidget = floatingDockContainer();
        if (FloatingWidget) {
            FloatingWidget->updateWindowTitle();
        }
        Q_EMIT titleChanged(title);
    } break;

    default:
        break;
    }

    return Super::event(e);
}

#ifndef QT_NO_TOOLTIP

void DockWidget::setTabToolTip(const QString &text)
{
    if (d->TabWidget) {
        d->TabWidget->setToolTip(text);
    }
    if (d->ToggleViewAction) {
        d->ToggleViewAction->setToolTip(text);
    }
    if (d->dockArea) {
        d->dockArea->markTitleBarMenuOutdated();   // update tabs menu
    }
}
#endif

void DockWidget::setIcon(const QIcon &Icon)
{
    d->TabWidget->setIcon(Icon);

    if (d->SideTabWidget) {
        d->SideTabWidget->setIcon(Icon);
    }

    if (!d->ToggleViewAction->isCheckable()) {
        d->ToggleViewAction->setIcon(Icon);
    }
}

QIcon DockWidget::icon() const
{
    return d->TabWidget->icon();
}

QToolBar *DockWidget::toolBar() const
{
    return d->ToolBar;
}

QToolBar *DockWidget::createDefaultToolBar()
{
    if (!d->ToolBar) {
        d->setupToolBar();
    }

    return d->ToolBar;
}

void DockWidget::setToolBar(QToolBar *ToolBar)
{
    if (d->ToolBar) {
        delete d->ToolBar;
    }

    d->ToolBar = ToolBar;
    d->Layout->insertWidget(0, d->ToolBar);
    this->connect(this, SIGNAL(topLevelChanged(bool)), SLOT(setToolbarFloatingStyle(bool)));
    setToolbarFloatingStyle(isFloating());
}

void DockWidget::setToolBarStyle(Qt::ToolButtonStyle Style, eState State)
{
    if (StateFloating == State) {
        d->ToolBarStyleFloating = Style;
    } else {
        d->ToolBarStyleDocked = Style;
    }

    setToolbarFloatingStyle(isFloating());
}

Qt::ToolButtonStyle DockWidget::toolBarStyle(eState State) const
{
    if (StateFloating == State) {
        return d->ToolBarStyleFloating;
    } else {
        return d->ToolBarStyleDocked;
    }
}

void DockWidget::setToolBarIconSize(const QSize &IconSize, eState State)
{
    if (StateFloating == State) {
        d->ToolBarIconSizeFloating = IconSize;
    } else {
        d->ToolBarIconSizeDocked = IconSize;
    }

    setToolbarFloatingStyle(isFloating());
}

QSize DockWidget::toolBarIconSize(eState State) const
{
    if (StateFloating == State) {
        return d->ToolBarIconSizeFloating;
    } else {
        return d->ToolBarIconSizeDocked;
    }
}

void DockWidget::setToolbarFloatingStyle(bool Floating)
{
    if (!d->ToolBar) {
        return;
    }

    auto IconSize = Floating ? d->ToolBarIconSizeFloating : d->ToolBarIconSizeDocked;
    if (IconSize != d->ToolBar->iconSize()) {
        d->ToolBar->setIconSize(IconSize);
    }

    auto ButtonStyle = Floating ? d->ToolBarStyleFloating : d->ToolBarStyleDocked;
    if (ButtonStyle != d->ToolBar->toolButtonStyle()) {
        d->ToolBar->setToolButtonStyle(ButtonStyle);
    }
}

void DockWidget::emitTopLevelEventForWidget(DockWidget *TopLevelDockWidget, bool Floating)
{
    if (TopLevelDockWidget) {
        TopLevelDockWidget->dockAreaWidget()->updateTitleBarVisibility();
        TopLevelDockWidget->emitTopLevelChanged(Floating);
    }
}

void DockWidget::emitTopLevelChanged(bool Floating)
{
    if (Floating != d->IsFloatingTopLevel) {
        d->IsFloatingTopLevel = Floating;
        Q_EMIT topLevelChanged(d->IsFloatingTopLevel);
    }
}

void DockWidget::setClosedState(bool Closed)
{
    d->Closed = Closed;
}

QSize DockWidget::minimumSizeHint() const
{
    if (d->MinimumSizeHintMode == DockWidget::MinimumSizeHintFromDockWidget || !d->Widget) {
        return QSize(60, 40);
    } else {
        return d->Widget->minimumSizeHint();
    }
}

void DockWidget::setFloating()
{
    if (isClosed()) {
        return;
    }
    d->TabWidget->detachDockWidget();
}

void DockWidget::deleteDockWidget()
{
    auto manager = dockManager();
    if (manager) {
        manager->removeDockWidget(this);
    }
    deleteLater();
    d->Closed = true;
}

void DockWidget::closeDockWidget()
{
    closeDockWidgetInternal(true);
}

bool DockWidget::closeDockWidgetInternal(bool ForceClose)
{
    if (!ForceClose) {
        Q_EMIT closeRequested();
    }

    if (!ForceClose && features().testFlag(DockWidget::CustomCloseHandling)) {
        return false;
    }

    if (features().testFlag(DockWidget::DockWidgetDeleteOnClose)) {
        // If the dock widget is floating, then we check if we also need to
        // delete the floating widget
        if (isFloating()) {
            DockFloatingContainer *FloatingWidget = internal::findParent<DockFloatingContainer *>(this);
            if (FloatingWidget->dockWidgets().count() == 1) {
                FloatingWidget->deleteLater();
            } else {
                FloatingWidget->hide();
            }
        }
        if (d->dockArea && d->dockArea->isAutoHide()) {
            d->dockArea->autoHideDockContainer()->cleanupAndDelete();
        }
        deleteDockWidget();
        Q_EMIT closed();
    } else {
        toggleView(false);
    }

    return true;
}

void DockWidget::setTitleBarActions(QList<QAction *> actions)
{
    d->TitleBarActions = actions;
}

QList<QAction *> DockWidget::titleBarActions() const
{
    return d->TitleBarActions;
}

void DockWidget::showFullScreen()
{
    if (isFloating()) {
        dockContainer()->floatingWidget()->showFullScreen();
    } else {
        Super::showFullScreen();
    }
}

void DockWidget::showNormal()
{
    if (isFloating()) {
        dockContainer()->floatingWidget()->showNormal();
    } else {
        Super::showNormal();
    }
}

bool DockWidget::isFullScreen() const
{
    if (isFloating()) {
        return dockContainer()->floatingWidget()->isFullScreen();
    } else {
        return Super::isFullScreen();
    }
}

void DockWidget::setAsCurrentTab()
{
    if (d->dockArea && !isClosed()) {
        d->dockArea->setCurrentDockWidget(this);
    }
}

bool DockWidget::isTabbed() const
{
    return d->dockArea && (d->dockArea->openDockWidgetsCount() > 1);
}

bool DockWidget::isCurrentTab() const
{
    return d->dockArea && (d->dockArea->currentDockWidget() == this);
}

void DockWidget::raise()
{
    if (isClosed()) {
        return;
    }

    setAsCurrentTab();
    if (isInFloatingContainer()) {
        auto FloatingWindow = window();
        FloatingWindow->raise();
        FloatingWindow->activateWindow();
    }
}

void DockWidget::setAutoHide(bool Enable, SideBarLocation Location)
{
    if (!DockManager::testAutoHideConfigFlag(DockManager::AutoHideFeatureEnabled)) {
        return;
    }

    // Do nothing if nothing changes
    if (Enable == isAutoHide()) {
        return;
    }

    auto dockArea = dockAreaWidget();
    if (!Enable) {
        dockArea->setAutoHide(false);
    } else {
        auto area = (SideBarNone == Location) ? dockArea->calculateSideTabBarArea() : Location;
        dockContainer()->createAndSetupAutoHideContainer(area, this);
    }
}

void DockWidget::toggleAutoHide(SideBarLocation Location)
{
    if (!DockManager::testAutoHideConfigFlag(DockManager::AutoHideFeatureEnabled)) {
        return;
    }

    setAutoHide(!isAutoHide(), Location);
}

QX_END_NAMESPACE
