/**
 * Copyright (C) 2023 maminjie <canpool@163.com>
 * Copyright (C) 2019 Uwe Kindler
 * SPDX-License-Identifier: LGPL-2.1
 **/
#pragma once

#include "QxDockGlobal.h"
#include <QXmlStreamReader>

QX_BEGIN_NAMESPACE

/**
 * Extends QXmlStreamReader with file version information
 */
class DockStateReader : public QXmlStreamReader
{
private:
    int m_FileVersion;
public:
    using QXmlStreamReader::QXmlStreamReader;

    /**
     * Set the file version for this state reader
     */
    void setFileVersion(int FileVersion);

    /**
     * Returns the file version set via setFileVersion
     */
    int fileVersion() const;
};

QX_END_NAMESPACE
