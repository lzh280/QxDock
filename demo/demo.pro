QT += core gui widgets

OUT_ROOT = $${OUT_PWD}/..
TARGET = DockDemo
DESTDIR = $${OUT_ROOT}/bin

lessThan(QT_MAJOR_VERSION, 6) {
    win32 {
        QT += axcontainer
    }
}

CONFIG += c++14
CONFIG += debug_and_release
DEFINES += QT_DEPRECATED_WARNINGS
RC_FILE += app.rc

QxDockBuildStatic {
    DEFINES += QX_DOCK_STATIC
}


HEADERS += \
    MainWindow.h \
    StatusDialog.h \
    ImageViewer.h \
    RenderWidget.h

SOURCES += \
    main.cpp \
    MainWindow.cpp \
    StatusDialog.cpp \
    ImageViewer.cpp \
    RenderWidget.cpp

FORMS += \
    mainwindow.ui \
    StatusDialog.ui

RESOURCES += demo.qrc

include($$PWD/../common.pri)
LIBS += -L$${OUT_ROOT}/bin -l$$qtLibraryNameVersion(QxDock, 1)
