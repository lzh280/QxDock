
/*******************************************************************************
** Qt Advanced Docking System
** Copyright (C) 2017 Uwe Kindler
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Lesser General Public
** License as published by the Free Software Foundation; either
** version 2.1 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
** Lesser General Public License for more details.
**
** You should have received a copy of the GNU Lesser General Public
** License along with this library; If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/



/// \file   MainWindow.cpp
/// \author Uwe Kindler
/// \date   13.02.2018
/// \brief  Implementation of CMainWindow demo class




#include <MainWindow.h>
#include "ui_mainwindow.h"

#include <iostream>

#include <QTime>
#include <QLabel>
#include <QTextEdit>
#include <QCalendarWidget>
#include <QFrame>
#include <QTreeView>
#include <QFileSystemModel>
#include <QBoxLayout>
#include <QSettings>
#include <QDockWidget>
#include <QDebug>
#include <QResizeEvent>
#include <QAction>
#include <QWidgetAction>
#include <QComboBox>
#include <QInputDialog>
#include <QRubberBand>
#include <QPlainTextEdit>
#include <QTableWidget>
#include <QScreen>
#include <QStyle>
#include <QMessageBox>
#include <QMenu>
#include <QToolButton>
#include <QToolBar>
#include <QPointer>
#include <QMap>
#include <QElapsedTimer>


#if QT_VERSION >= QT_VERSION_CHECK(5, 10, 0)
#include <QRandomGenerator>
#endif

#ifdef Q_OS_WIN
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
#include <QAxWidget>
#endif
#endif

#include "QxDockManager.h"
#include "QxDockWidget.h"
#include "QxDockAreaWidget.h"
#include "QxDockAreaTitleBar.h"
#include "QxDockAreaTabBar.h"
#include "QxDockFloatingContainer.h"
#include "QxDockFactory.h"
#include "StatusDialog.h"
#include "QxDockSplitter.h"
#include "ImageViewer.h"



/**
 * Returns a random number from 0 to highest - 1
 */
int randomNumberBounded(int highest)
{
#if QT_VERSION >= QT_VERSION_CHECK(5, 10, 0)
	return QRandomGenerator::global()->bounded(highest);
#else
	qsrand(QTime::currentTime().msec());
	return qrand() % highest;
#endif
}


/**
 * Function returns a features string with closable (c), movable (m) and floatable (f)
 * features. i.e. The following string is for a not closable but movable and floatable
 * widget: c- m+ f+
 */
static QString featuresString(Qx::DockWidget* dockWidget)
{
	auto f = dockWidget->features();
	return QString("c%1 m%2 f%3")
		.arg(f.testFlag(Qx::DockWidget::DockWidgetClosable) ? "+" : "-")
		.arg(f.testFlag(Qx::DockWidget::DockWidgetMovable) ? "+" : "-")
		.arg(f.testFlag(Qx::DockWidget::DockWidgetFloatable) ? "+" : "-");
}


/**
 * Appends the string returned by featuresString() to the window title of
 * the given dockWidget
 */
static void appendFeaturStringToWindowTitle(Qx::DockWidget* dockWidget)
{
	dockWidget->setWindowTitle(dockWidget->windowTitle()
		+  QString(" (%1)").arg(featuresString(dockWidget)));
}

/**
 * Helper function to create an SVG icon
 */
static QIcon svgIcon(const QString& File)
{
	// This is a workaround, because in item views SVG icons are not
	// properly scaled and look blurry or pixelate
	QIcon SvgIcon(File);
	SvgIcon.addPixmap(SvgIcon.pixmap(92));
	return SvgIcon;
}



class CCustomComponentsFactory : public Qx::DockComponentsFactory
{
public:
	using Super = Qx::DockComponentsFactory;
	Qx::DockAreaTitleBar* createDockAreaTitleBar(Qx::DockAreaWidget* dockArea) const override
	{
		auto TitleBar = new Qx::DockAreaTitleBar(dockArea);
		auto CustomButton = new QToolButton(dockArea);
		CustomButton->setToolTip(QObject::tr("Help"));
		CustomButton->setIcon(svgIcon(":/adsdemo/images/help_outline.svg"));
		CustomButton->setAutoRaise(true);
		int Index = TitleBar->indexOf(TitleBar->button(Qx::TitleBarButtonTabsMenu));
		TitleBar->insertWidget(Index + 1, CustomButton);
		return TitleBar;
	}
};



/**
 * Custom QTableWidget with a minimum size hint to test DockWidget
 * setMinimumSizeHintMode() function of DockWidget
 */
class CMinSizeTableWidget : public QTableWidget
{
public:
	using QTableWidget::QTableWidget;
	virtual QSize minimumSizeHint() const override
	{
		return QSize(300, 100);
	}
};




/**
 * Private data class pimpl
 */
struct MainWindowPrivate
{
	CMainWindow* _this;
	Ui::MainWindow ui;
	QAction* SavePerspectiveAction = nullptr;
	QWidgetAction* PerspectiveListAction = nullptr;
	QComboBox* PerspectiveComboBox = nullptr;
	Qx::DockManager* dockManager = nullptr;
	Qx::DockWidget* WindowTitleTestDockWidget = nullptr;
	QPointer<Qx::DockWidget> LastDockedEditor;
	QPointer<Qx::DockWidget> LastCreatedFloatingEditor;

	MainWindowPrivate(CMainWindow* _public) : _this(_public) {}

	/**
	 * Creates the toolbar actions
	 */
	void createActions();

	/**
	 * Fill the dock manager with dock widgets
	 */
	void createContent();

	/**
	 * Saves the dock manager state and the main window geometry
	 */
	void saveState();

	/**
	 * Save the list of perspectives
	 */
	void savePerspectives();

	/**
	 * Restores the dock manager state
	 */
	void restoreState();

	/**
	 * Restore the perspective listo of the dock manager
	 */
	void restorePerspectives();

	/**
	 * Creates a dock widget with a file system tree view
	 */
	Qx::DockWidget* createFileSystemTreeDockWidget()
	{
		static int FileSystemCount = 0;
		QTreeView* w = new QTreeView();
		w->setFrameShape(QFrame::NoFrame);
		QFileSystemModel* m = new QFileSystemModel(w);
		m->setRootPath(QDir::currentPath());
		w->setModel(m);
		w->setRootIndex(m->index(QDir::currentPath()));
		Qx::DockWidget* dockWidget = new Qx::DockWidget(QString("Filesystem %1")
			.arg(FileSystemCount++));
		dockWidget->setWidget(w);
		dockWidget->setIcon(svgIcon(":/adsdemo/images/folder_open.svg"));
		ui.menuView->addAction(dockWidget->toggleViewAction());
		// We disable focus to test focus highlighting if the dock widget content
		// does not support focus
		w->setFocusPolicy(Qt::NoFocus);
		auto ToolBar = dockWidget->createDefaultToolBar();
		ToolBar->addAction(ui.actionSaveState);
		ToolBar->addAction(ui.actionRestoreState);
		return dockWidget;
	}

	/**
	 * Create a dock widget with a QCalendarWidget
	 */
	Qx::DockWidget* createCalendarDockWidget()
	{
		static int CalendarCount = 0;
		QCalendarWidget* w = new QCalendarWidget();
		Qx::DockWidget* dockWidget = new Qx::DockWidget(QString("Calendar %1").arg(CalendarCount++));
		// The following lines are for testing the setWidget() and takeWidget()
		// functionality
		dockWidget->setWidget(w);
		dockWidget->setWidget(w); // what happens if we set a widget if a widget is already set
		dockWidget->takeWidget(); // we remove the widget
		dockWidget->setWidget(w); // and set the widget again - there should be no error
		dockWidget->setToggleViewActionMode(Qx::DockWidget::ActionModeShow);
		dockWidget->setIcon(svgIcon(":/adsdemo/images/date_range.svg"));
		ui.menuView->addAction(dockWidget->toggleViewAction());
		auto ToolBar = dockWidget->createDefaultToolBar();
		ToolBar->addAction(ui.actionSaveState);
		ToolBar->addAction(ui.actionRestoreState);
		return dockWidget;
	}


	/**
	 * Create dock widget with a text label
	 */
	Qx::DockWidget* createLongTextLabelDockWidget()
	{
		static int LabelCount = 0;
		QLabel* l = new QLabel();
		l->setWordWrap(true);
		l->setAlignment(Qt::AlignTop | Qt::AlignLeft);
		l->setText(QString("Label %1 %2 - Lorem ipsum dolor sit amet, consectetuer adipiscing elit. "
			"Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque "
			"penatibus et magnis dis parturient montes, nascetur ridiculus mus. "
			"Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. "
			"Nulla consequat massa quis enim. Donec pede justo, fringilla vel, "
			"aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, "
			"imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede "
			"mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum "
			"semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, "
			"porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, "
			"dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla "
			"ut metus varius laoreet.")
			.arg(LabelCount)
			.arg(QTime::currentTime().toString("hh:mm:ss:zzz")));

		Qx::DockWidget* dockWidget = new Qx::DockWidget(QString("Label %1").arg(LabelCount++));
		dockWidget->setWidget(l);
		dockWidget->setIcon(svgIcon(":/adsdemo/images/font_download.svg"));
		ui.menuView->addAction(dockWidget->toggleViewAction());
		return dockWidget;
	}


	/**
	 * Creates as imple editor widget
	 */
	Qx::DockWidget* createEditorWidget()
	{
		static int EditorCount = 0;
		QPlainTextEdit* w = new QPlainTextEdit();
		w->setPlaceholderText("This is an editor. If you close the editor, it will be "
			"deleted. Enter your text here.");
		w->setStyleSheet("border: none");
		Qx::DockWidget* dockWidget = new Qx::DockWidget(QString("Editor %1").arg(EditorCount++));
		dockWidget->setWidget(w);
		dockWidget->setIcon(svgIcon(":/adsdemo/images/edit.svg"));
		dockWidget->setFeature(Qx::DockWidget::CustomCloseHandling, true);
		ui.menuView->addAction(dockWidget->toggleViewAction());

		QMenu* OptionsMenu = new QMenu(dockWidget);
		OptionsMenu->setTitle(QObject::tr("Options"));
		OptionsMenu->setToolTip(OptionsMenu->title());
		OptionsMenu->setIcon(svgIcon(":/adsdemo/images/custom-menu-button.svg"));
		auto MenuAction = OptionsMenu->menuAction();
		// The object name of the action will be set for the QToolButton that
		// is created in the dock area title bar. You can use this name for CSS
		// styling
		MenuAction->setObjectName("optionsMenu");
		dockWidget->setTitleBarActions({OptionsMenu->menuAction()});
		auto a = OptionsMenu->addAction(QObject::tr("Clear Editor"));
		w->connect(a, SIGNAL(triggered()), SLOT(clear()));

		return dockWidget;
	}

	/**
	 * Creates a simply image viewr
	 */
	Qx::DockWidget* createImageViewer()
	{
		static int ImageViewerCount = 0;
		auto w = new CImageViewer();
		auto ImageIndex = randomNumberBounded(4);
		auto FileName = ":adsdemo/images/ads_logo.svg";

		// Pick a random image from a number of images
		switch (ImageIndex)
		{
		case 0: FileName = ":adsdemo/images/ads_tile_blue.svg"; break;
		case 1: FileName = ":adsdemo/images/ads_tile_blue_light.svg"; break;
		case 2: FileName = ":adsdemo/images/ads_tile_green.svg"; break;
		case 3: FileName = ":adsdemo/images/ads_tile_orange.svg"; break;
		}

		auto Result = w->loadFile(FileName);
		qDebug() << "loadFile result: " << Result;
		Qx::DockWidget* dockWidget = new Qx::DockWidget(QString("Image Viewer %1").arg(ImageViewerCount++));
		dockWidget->setIcon(svgIcon(":/adsdemo/images/photo.svg"));
		dockWidget->setWidget(w,Qx:: DockWidget::ForceNoScrollArea);
		auto ToolBar = dockWidget->createDefaultToolBar();
		ToolBar->addActions(w->actions());
		return dockWidget;
	}

	/**
	 * Create a table widget
	 */
	Qx::DockWidget* createTableWidget()
	{
		static int TableCount = 0;
		auto w = new CMinSizeTableWidget();
		Qx::DockWidget* dockWidget = new Qx::DockWidget(QString("Table %1").arg(TableCount++));
		static int colCount = 5;
		static int rowCount = 30;
		w->setColumnCount(colCount);
		w->setRowCount(rowCount);
		for (int col = 0; col < colCount; ++col)
		{
		  w->setHorizontalHeaderItem(col, new QTableWidgetItem(QString("Col %1").arg(col+1)));
		  for (int row = 0; row < rowCount; ++row)
		  {
			 w->setItem(row, col, new QTableWidgetItem(QString("T %1-%2").arg(row + 1).arg(col+1)));
		  }
		}
		dockWidget->setWidget(w);
		dockWidget->setIcon(svgIcon(":/adsdemo/images/grid_on.svg"));
		dockWidget->setMinimumSizeHintMode(Qx::DockWidget::MinimumSizeHintFromContent);
		auto ToolBar = dockWidget->createDefaultToolBar();
		auto Action = ToolBar->addAction(svgIcon(":/adsdemo/images/fullscreen.svg"), "Toggle Fullscreen");
		QObject::connect(Action, &QAction::triggered, [=]()
			{
				if (dockWidget->isFullScreen())
				{
					dockWidget->showNormal();
				}
				else
				{
					dockWidget->showFullScreen();
				}
			});
		ui.menuView->addAction(dockWidget->toggleViewAction());
		return dockWidget;
	}


#ifdef Q_OS_WIN
#if (QT_VERSION < QT_VERSION_CHECK(6, 0, 0))
	/**
	 * Creates an ActiveX widget on windows
	 */
	Qx::DockWidget* createActiveXWidget(QWidget* parent = nullptr)
	{
	   static int ActiveXCount = 0;
	   QAxWidget* w = new QAxWidget("{6bf52a52-394a-11d3-b153-00c04f79faa6}", parent);
	   Qx::DockWidget* dockWidget = new Qx::DockWidget(QString("Active X %1").arg(ActiveXCount++));
	   dockWidget->setWidget(w);
	   ui.menuView->addAction(dockWidget->toggleViewAction());
	   return dockWidget;
	}
#endif
#endif

};


void MainWindowPrivate::createContent()
{
	// Test container docking
	auto dockWidget = createCalendarDockWidget();
	dockWidget->setFeature(Qx::DockWidget::DockWidgetClosable, false);
	auto SpecialDockArea = dockManager->addDockWidget(Qx::LeftDockWidgetArea, dockWidget);

	// For this Special Dock Area we want to avoid dropping on the center of it (i.e. we don't want this widget to be ever tabbified):
	{
		SpecialDockArea->setAllowedAreas(Qx::OuterDockAreas);
		//SpecialDockArea->setAllowedAreas({Qx::LeftDockWidgetArea, Qx::RightDockWidgetArea}); // just for testing
	}

	dockWidget = createLongTextLabelDockWidget();
	WindowTitleTestDockWidget = dockWidget;
	dockWidget->setFeature(Qx::DockWidget::DockWidgetFocusable, false);
	dockManager->addDockWidget(Qx::LeftDockWidgetArea, dockWidget);
	auto FileSystemWidget = createFileSystemTreeDockWidget();
	FileSystemWidget->setFeature(Qx::DockWidget::DockWidgetFloatable, false);
	appendFeaturStringToWindowTitle(FileSystemWidget);
	dockManager->addDockWidget(Qx::BottomDockWidgetArea, FileSystemWidget);

	FileSystemWidget = createFileSystemTreeDockWidget();
	FileSystemWidget->setFeature(Qx::DockWidget::DockWidgetMovable, false);
	FileSystemWidget->setFeature(Qx::DockWidget::DockWidgetFloatable, false);
	appendFeaturStringToWindowTitle(FileSystemWidget);

	// Test custom factory - we inject a help button into the title bar
	Qx::DockComponentsFactory::setFactory(new CCustomComponentsFactory());
	auto TopDockArea = dockManager->addDockWidget(Qx::TopDockWidgetArea, FileSystemWidget);
	// Uncomment the next line if you would like to test the
	// HideSingleWidgetTitleBar functionality
	// TopDockArea->setDockAreaFlag(Qx::DockAreaWidget::HideSingleWidgetTitleBar, true);
	Qx::DockComponentsFactory::resetDefaultFactory();

	// We create a calendar widget and clear all flags to prevent the dock area
	// from closing
	dockWidget = createCalendarDockWidget();
	dockWidget->setTabToolTip(QString("Tab ToolTip\nHodie est dies magna"));
	auto dockArea = dockManager->addDockWidget(Qx::CenterDockWidgetArea, dockWidget, TopDockArea);
    // Now we create a action to test resizing of dockArea widget
	auto Action = ui.menuTests->addAction(QString("Resize %1").arg(dockWidget->windowTitle()));
	QObject::connect(Action, &QAction::triggered, [dockArea]()
	{
		// Resizing only works, if the Splitter is visible and has a valid
		// sizes
		auto Splitter = Qx::internal::findParent<Qx::DockSplitter*>(dockArea);
		if (!Splitter)
		{
			return;
		}
		// We change the sizes of the splitter that contains the Calendar 1 widget
		// to resize the dock widget
		int Width = Splitter->width();
		Splitter->setSizes({Width * 2/3, Width * 1/3});
	});
	dockWidget->setWindowTitle(QString("My " + dockWidget->windowTitle()));

	// Now we add a custom button to the dock area title bar that will create
	// new editor widgets when clicked
	auto CustomButton = new QToolButton(dockArea);
	CustomButton->setToolTip(QObject::tr("Create Editor"));
	CustomButton->setIcon(svgIcon(":/adsdemo/images/plus.svg"));
	CustomButton->setAutoRaise(true);

	auto TitleBar = dockArea->titleBar();
	int Index = TitleBar->indexOf(TitleBar->tabBar());
	TitleBar->insertWidget(Index + 1, CustomButton);
	QObject::connect(CustomButton, &QToolButton::clicked, [=]()
	{
		auto dockWidget = createEditorWidget();
		dockWidget->setFeature(Qx::DockWidget::DockWidgetDeleteOnClose, true);
		dockManager->addDockWidgetTabToArea(dockWidget, dockArea);
		_this->connect(dockWidget, SIGNAL(closeRequested()), SLOT(onEditorCloseRequested()));
	});

	// Test dock area docking
	auto RighDockArea = dockManager->addDockWidget(Qx::RightDockWidgetArea, createLongTextLabelDockWidget(), TopDockArea);
	dockManager->addDockWidget(Qx::TopDockWidgetArea, createLongTextLabelDockWidget(), RighDockArea);
	auto BottomDockArea = dockManager->addDockWidget(Qx::BottomDockWidgetArea, createLongTextLabelDockWidget(), RighDockArea);
	dockManager->addDockWidget(Qx::CenterDockWidgetArea, createLongTextLabelDockWidget(), RighDockArea);
	auto LabelDockWidget = createLongTextLabelDockWidget();
	dockManager->addDockWidget(Qx::CenterDockWidgetArea, LabelDockWidget, BottomDockArea);

	// Tests CustomCloseHandling without DeleteOnClose
	LabelDockWidget->setFeature(Qx::DockWidget::CustomCloseHandling, true);
	QObject::connect(LabelDockWidget, &Qx::DockWidget::closeRequested, [LabelDockWidget, this]()
	{
		int Result = QMessageBox::question(_this, "Custom Close Request",
			"Do you really want to close this dock widget?");
		if (QMessageBox::Yes == Result)
		{
			LabelDockWidget->closeDockWidget();
		}
	});

    Action = ui.menuTests->addAction(QString("Set %1 Floating").arg(dockWidget->windowTitle()));
    dockWidget->connect(Action, SIGNAL(triggered()), SLOT(setFloating()));
    Action = ui.menuTests->addAction(QString("Set %1 As Current Tab").arg(dockWidget->windowTitle()));
    dockWidget->connect(Action, SIGNAL(triggered()), SLOT(setAsCurrentTab()));
    Action = ui.menuTests->addAction(QString("Raise %1").arg(dockWidget->windowTitle()));
    dockWidget->connect(Action, SIGNAL(triggered()), SLOT(raise()));

    // Test hidden floating dock widget
    dockWidget = createLongTextLabelDockWidget();
    dockManager->addDockWidgetFloating(dockWidget);
    dockWidget->toggleView(false);

    // Test visible floating dock widget
    dockWidget = createCalendarDockWidget();
    dockManager->addDockWidgetFloating(dockWidget);
    dockWidget->setWindowTitle(QString("My " + dockWidget->windowTitle()));


#ifdef Q_OS_WIN
#if (QT_VERSION < QT_VERSION_CHECK(6, 0, 0))
    dockManager->addDockWidget(Qx::CenterDockWidgetArea, createActiveXWidget(), RighDockArea);
#endif
#endif

	for (auto dockWidget : dockManager->dockWidgetsMap())
	{
		_this->connect(dockWidget, SIGNAL(viewToggled(bool)), SLOT(onViewToggled(bool)));
		_this->connect(dockWidget, SIGNAL(visibilityChanged(bool)), SLOT(onViewVisibilityChanged(bool)));
	}

	// Create image viewer
	dockWidget = createImageViewer();
	dockManager->addDockWidget(Qx::LeftDockWidgetArea, dockWidget);
}



void MainWindowPrivate::createActions()
{
	ui.toolBar->addAction(ui.actionSaveState);
	ui.toolBar->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
	ui.actionSaveState->setIcon(svgIcon(":/adsdemo/images/save.svg"));
	ui.toolBar->addAction(ui.actionRestoreState);
	ui.actionRestoreState->setIcon(svgIcon(":/adsdemo/images/restore.svg"));

	SavePerspectiveAction = new QAction("Create Perspective", _this);
	SavePerspectiveAction->setIcon(svgIcon(":/adsdemo/images/picture_in_picture.svg"));
	_this->connect(SavePerspectiveAction, SIGNAL(triggered()), SLOT(savePerspective()));
	PerspectiveListAction = new QWidgetAction(_this);
	PerspectiveComboBox = new QComboBox(_this);
	PerspectiveComboBox->setSizeAdjustPolicy(QComboBox::AdjustToContents);
	PerspectiveComboBox->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
	PerspectiveListAction->setDefaultWidget(PerspectiveComboBox);
	ui.toolBar->addSeparator();
	ui.toolBar->addAction(PerspectiveListAction);
	ui.toolBar->addAction(SavePerspectiveAction);

	QAction* a = ui.toolBar->addAction("Create Floating Editor");
	a->setProperty("Floating", true);
	a->setToolTip("Creates floating dynamic dockable editor windows that are deleted on close");
	a->setIcon(svgIcon(":/adsdemo/images/note_add.svg"));
	_this->connect(a, SIGNAL(triggered()), SLOT(createEditor()));
	ui.menuTests->addAction(a);

	a = ui.toolBar->addAction("Create Docked Editor");
	a->setProperty("Floating", false);
	a->setToolTip("Creates a docked editor windows that are deleted on close");
	a->setIcon(svgIcon(":/adsdemo/images/docked_editor.svg"));
	_this->connect(a, SIGNAL(triggered()), SLOT(createEditor()));
	ui.menuTests->addAction(a);

	a = ui.toolBar->addAction("Create Editor Tab");
	a->setProperty("Floating", false);
	a->setToolTip("Creates a editor tab and inserts it as second tab into an area");
	a->setIcon(svgIcon(":/adsdemo/images/tab.svg"));
	a->setProperty("Tabbed", true);
	_this->connect(a, SIGNAL(triggered()), SLOT(createEditor()));
	ui.menuTests->addAction(a);

	a = ui.toolBar->addAction("Create Floating Table");
	a->setToolTip("Creates floating dynamic dockable table with millions of entries");
	a->setIcon(svgIcon(":/adsdemo/images/grid_on.svg"));
	_this->connect(a, SIGNAL(triggered()), SLOT(createTable()));
	ui.menuTests->addAction(a);

	a = ui.toolBar->addAction("Create Image Viewer");
	auto ToolButton = qobject_cast<QToolButton*>(ui.toolBar->widgetForAction(a));
	ToolButton->setPopupMode(QToolButton::InstantPopup);
	a->setToolTip("Creates floating, docked or pinned image viewer");
	a->setIcon(svgIcon(":/adsdemo/images/panorama.svg"));
	ui.menuTests->addAction(a);
	auto Menu = new QMenu();
	ToolButton->setMenu(Menu);
	a = Menu->addAction("Floating Image Viewer");
	_this->connect(a, SIGNAL(triggered()), SLOT(createImageViewer()));
	a = Menu->addAction("Docked Image Viewer");
	_this->connect(a, SIGNAL(triggered()), SLOT(createImageViewer()));
	a = Menu->addAction("Pinned Image Viewer");
	_this->connect(a, SIGNAL(triggered()), SLOT(createImageViewer()));


	ui.menuTests->addSeparator();
	a = ui.menuTests->addAction("Show Status Dialog");
	_this->connect(a, SIGNAL(triggered()), SLOT(showStatusDialog()));

	a = ui.menuTests->addAction("Toggle Label 0 Window Title");
	_this->connect(a, SIGNAL(triggered()), SLOT(toggleDockWidgetWindowTitle()));
	ui.menuTests->addSeparator();

	a = ui.toolBar->addAction("Apply VS Style");
	a->setToolTip("Applies a Visual Studio light style (visual_studio_light.css)." );
	a->setIcon(svgIcon(":/adsdemo/images/color_lens.svg"));
	QObject::connect(a, &QAction::triggered, _this, &CMainWindow::applyVsStyle);
	ui.menuTests->addAction(a);
}



void MainWindowPrivate::saveState()
{
	QSettings Settings("Settings.ini", QSettings::IniFormat);
	Settings.setValue("mainWindow/Geometry", _this->saveGeometry());
	Settings.setValue("mainWindow/State", _this->saveState());
	Settings.setValue("mainWindow/DockingState", dockManager->saveState());
}



void MainWindowPrivate::restoreState()
{
	QSettings Settings("Settings.ini", QSettings::IniFormat);
	_this->restoreGeometry(Settings.value("mainWindow/Geometry").toByteArray());
	_this->restoreState(Settings.value("mainWindow/State").toByteArray());
	dockManager->restoreState(Settings.value("mainWindow/DockingState").toByteArray());
}




void MainWindowPrivate::savePerspectives()
{
	QSettings Settings("Settings.ini", QSettings::IniFormat);
	dockManager->savePerspectives(Settings);
}




void MainWindowPrivate::restorePerspectives()
{
	QSettings Settings("Settings.ini", QSettings::IniFormat);
	dockManager->loadPerspectives(Settings);
	PerspectiveComboBox->clear();
	PerspectiveComboBox->addItems(dockManager->perspectiveNames());
}



CMainWindow::CMainWindow(QWidget *parent) :
	QMainWindow(parent),
	d(new MainWindowPrivate(this))
{
	using namespace Qx;
	d->ui.setupUi(this);
	setWindowTitle(QApplication::instance()->applicationName());
	d->createActions();

	// uncomment the following line if the tab close button should be
	// a QToolButton instead of a QPushButton
	// DockManager::setConfigFlags(DockManager::configFlags() | DockManager::TabCloseButtonIsToolButton);

    // uncomment the following line if you want to use opaque undocking and
	// opaque splitter resizing
    //DockManager::setConfigFlags(DockManager::DefaultOpaqueConfig);

    // uncomment the following line if you want a fixed tab width that does
	// not change if the visibility of the close button changes
    //DockManager::setConfigFlag(DockManager::RetainTabSizeWhenCloseButtonHidden, true);

	// uncomment the following line if you don't want close button on dockArea's title bar
	//DockManager::setConfigFlag(DockManager::DockAreaHasCloseButton, false);

	// uncomment the following line if you don't want undock button on dockArea's title bar
	//DockManager::setConfigFlag(DockManager::DockAreaHasUndockButton, false);

	// uncomment the following line if you don't want tabs menu button on dockArea's title bar
	//DockManager::setConfigFlag(DockManager::DockAreaHasTabsMenuButton, false);

	// uncomment the following line if you don't want disabled buttons to appear on dockArea's title bar
	//DockManager::setConfigFlag(DockManager::DockAreaHideDisabledButtons, true);

	// uncomment the following line if you want to show tabs menu button on dockArea's title bar only when there are more than one tab and at least of them has elided title
	//DockManager::setConfigFlag(DockManager::DockAreaDynamicTabsMenuButtonVisibility, true);

	// uncomment the following line if you want floating container to always show application title instead of active dock widget's title
	//DockManager::setConfigFlag(DockManager::FloatingContainerHasWidgetTitle, false);

	// uncomment the following line if you want floating container to show active dock widget's icon instead of always showing application icon
	//DockManager::setConfigFlag(DockManager::FloatingContainerHasWidgetIcon, true);

	// uncomment the following line if you want a central widget in the main dock container (the dock manager) without a titlebar
	// If you enable this code, you can test it in the demo with the Calendar 0
	// dock widget.
	//DockManager::setConfigFlag(DockManager::HideSingleCentralWidgetTitleBar, true);

	// uncomment the following line to enable focus highlighting of the dock
	// widget that has the focus
    DockManager::setConfigFlag(DockManager::FocusHighlighting, true);

	// uncomment if you would like to enable dock widget auto hiding
    DockManager::setAutoHideConfigFlags({DockManager::DefaultAutoHideConfig | DockManager::AutoHideCloseButtonCollapsesDock});

	// uncomment if you would like to enable an equal distribution of the
	// available size of a splitter to all contained dock widgets
	// DockManager::setConfigFlag(DockManager::EqualSplitOnInsertion, true);

	// uncomment if you would like to close tabs with the middle mouse button, web browser style
	// DockManager::setConfigFlag(DockManager::MiddleMouseButtonClosesTab, true);

	// Now create the dock manager and its content
	d->dockManager = new DockManager(this);

 #if (QT_VERSION < QT_VERSION_CHECK(6, 0, 0))
	connect(d->PerspectiveComboBox, SIGNAL(activated(QString)),
		d->dockManager, SLOT(openPerspective(QString)));
 #else
    connect(d->PerspectiveComboBox, SIGNAL(textActivated(QString)),
        d->dockManager, SLOT(openPerspective(QString)));
 #endif

	d->createContent();
	// Default window geometry - center on screen
    resize(1280, 720);
    setGeometry(QStyle::alignedRect(
        Qt::LeftToRight, Qt::AlignCenter, frameSize(),
        QGuiApplication::primaryScreen()->availableGeometry()
    ));

	//d->restoreState();
	d->restorePerspectives();
}



CMainWindow::~CMainWindow()
{
	delete d;
}



void CMainWindow::closeEvent(QCloseEvent* event)
{
	d->saveState();
    // Delete dock manager here to delete all floating widgets. This ensures
    // that all top level windows of the dock manager are properly closed
    d->dockManager->deleteLater();
	QMainWindow::closeEvent(event);
}



void CMainWindow::on_actionSaveState_triggered(bool)
{
	qDebug() << "MainWindow::on_actionSaveState_triggered";
	d->saveState();
}



void CMainWindow::on_actionRestoreState_triggered(bool)
{
	qDebug() << "MainWindow::on_actionRestoreState_triggered";
	d->restoreState();
}



void CMainWindow::savePerspective()
{
	QString PerspectiveName = QInputDialog::getText(this, "Save Perspective", "Enter unique name:");
	if (PerspectiveName.isEmpty())
	{
		return;
	}

	d->dockManager->addPerspective(PerspectiveName);
	QSignalBlocker Blocker(d->PerspectiveComboBox);
	d->PerspectiveComboBox->clear();
	d->PerspectiveComboBox->addItems(d->dockManager->perspectiveNames());
	d->PerspectiveComboBox->setCurrentText(PerspectiveName);

	d->savePerspectives();
}



void CMainWindow::onViewToggled(bool Open)
{
	Q_UNUSED(Open);
	auto dockWidget = qobject_cast<Qx::DockWidget*>(sender());
	if (!dockWidget)
	{
		return;
	}

	//qDebug() << dockWidget->objectName() << " viewToggled(" << Open << ")";
}



void CMainWindow::onViewVisibilityChanged(bool Visible)
{
	Q_UNUSED(Visible);
	auto dockWidget = qobject_cast<Qx::DockWidget*>(sender());
    if (!dockWidget)
    {
        return;
    }

    //qDebug() << dockWidget->objectName() << " visibilityChanged(" << Visible << ")";
}



void CMainWindow::createEditor()
{
	QObject* Sender = sender();
	QVariant vFloating = Sender->property("Floating");
	bool Floating = vFloating.isValid() ? vFloating.toBool() : true;
	QVariant vTabbed = Sender->property("Tabbed");
	bool Tabbed = vTabbed.isValid() ? vTabbed.toBool() : true;
	auto dockWidget = d->createEditorWidget();
	dockWidget->setFeature(Qx::DockWidget::DockWidgetDeleteOnClose, true);
	dockWidget->setFeature(Qx::DockWidget::DockWidgetForceCloseWithArea, true);
    connect(dockWidget, SIGNAL(closeRequested()), SLOT(onEditorCloseRequested()));

    if (Floating)
    {
		auto FloatingWidget = d->dockManager->addDockWidgetFloating(dockWidget);
		FloatingWidget->move(QPoint(20, 20));
		d->LastCreatedFloatingEditor = dockWidget;
		d->LastDockedEditor.clear();
		return;
    }


	Qx::DockAreaWidget* EditorArea = d->LastDockedEditor ? d->LastDockedEditor->dockAreaWidget() : nullptr;
	if (EditorArea)
	{
		if (Tabbed)
		{
			// Test inserting the dock widget tab at a given position instead
			// of appending it. This function inserts the new dock widget as
			// first tab
			d->dockManager->addDockWidgetTabToArea(dockWidget, EditorArea, 0);
		}
		else
		{
			d->dockManager->setConfigFlag(Qx::DockManager::EqualSplitOnInsertion, true);
			d->dockManager->addDockWidget(Qx::RightDockWidgetArea, dockWidget, EditorArea);
		}
	}
	else
	{
		if (d->LastCreatedFloatingEditor)
		{
			d->dockManager->addDockWidget(Qx::RightDockWidgetArea, dockWidget, d->LastCreatedFloatingEditor->dockAreaWidget());
		}
		else
		{
			d->dockManager->addDockWidget(Qx::TopDockWidgetArea, dockWidget);
		}
	}
	d->LastDockedEditor = dockWidget;
}



void CMainWindow::onEditorCloseRequested()
{
	auto dockWidget = qobject_cast<Qx::DockWidget*>(sender());
	int Result = QMessageBox::question(this, "Close Editor", QString("Editor %1 "
		"contains unsaved changes? Would you like to close it?")
		.arg(dockWidget->windowTitle()));
	if (QMessageBox::Yes == Result)
	{
		dockWidget->closeDockWidget();
	}
}



void CMainWindow::onImageViewerCloseRequested()
{
	auto dockWidget = qobject_cast<Qx::DockWidget*>(sender());
	int Result = QMessageBox::question(this, "Close Image Viewer", QString("%1 "
		"contains unsaved changes? Would you like to close it?")
		.arg(dockWidget->windowTitle()));
	if (QMessageBox::Yes == Result)
	{
		dockWidget->closeDockWidget();
	}
}



void CMainWindow::createTable()
{
	auto dockWidget = d->createTableWidget();
	dockWidget->setFeature(Qx::DockWidget::DockWidgetDeleteOnClose, true);
	auto FloatingWidget = d->dockManager->addDockWidgetFloating(dockWidget);
    FloatingWidget->move(QPoint(40, 40));
}



void CMainWindow::showStatusDialog()
{
	CStatusDialog Dialog(d->dockManager);
	Dialog.exec();
}



void CMainWindow::toggleDockWidgetWindowTitle()
{
	QString Title = d->WindowTitleTestDockWidget->windowTitle();
	int i = Title.indexOf(" (Test)");
	if (-1 == i)
	{
		Title += " (Test)";
	}
	else
	{
		Title = Title.left(i);
	}
	d->WindowTitleTestDockWidget->setWindowTitle(Title);
}



void CMainWindow::applyVsStyle()
{
	QFile StyleSheetFile(":adsdemo/res/visual_studio_light.css");
	StyleSheetFile.open(QIODevice::ReadOnly);
	QTextStream StyleSheetStream(&StyleSheetFile);
	auto Stylesheet = StyleSheetStream.readAll();
	StyleSheetFile.close();
	d->dockManager->setStyleSheet(Stylesheet);
}



void CMainWindow::createImageViewer()
{
	QAction* a = qobject_cast<QAction*>(sender());
	qDebug() << "createImageViewer " << a->text();

	auto dockWidget = d->createImageViewer();
	dockWidget->setFeature(Qx::DockWidget::DockWidgetDeleteOnClose, true);
	dockWidget->setFeature(Qx::DockWidget::DockWidgetForceCloseWithArea, true);
	dockWidget->setFeature(Qx::DockWidget::CustomCloseHandling, true);
	dockWidget->resize(QSize(640, 480));
	connect(dockWidget, &Qx::DockWidget::closeRequested, this,
		&CMainWindow::onImageViewerCloseRequested);

	if (a->text().startsWith("Floating"))
	{
		auto FloatingWidget = d->dockManager->addDockWidgetFloating(dockWidget);
		FloatingWidget->move(QPoint(20, 20));
	}
	else if (a->text().startsWith("Docked"))
	{
		d->dockManager->addDockWidget(Qx::RightDockWidgetArea, dockWidget);
	}
	else if (a->text().startsWith("Pinned"))
	{
		d->dockManager->addAutoHideDockWidget(Qx::SideBarLeft, dockWidget);
	}
}

