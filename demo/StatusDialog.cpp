
/// \file   StatusDialog.cpp
/// \author Uwe Kindler
/// \date   13.04.2020
/// \brief  Implementation of CStatusDialog class



#include "StatusDialog.h"

#include <iostream>

#include "QxDockManager.h"
#include "QxDockWidget.h"
#include "ui_StatusDialog.h"

/**
 * Private data class of CStatusDialog class (pimpl)
 */
struct StatusDialogPrivate
{
	CStatusDialog *_this;
	Ui::CStatusDialogClass ui;
	Qx::DockManager* dockManager;
	QMap<QString, Qx::DockWidget*> DockWidgets;

	/**
	 * Private data constructor
	 */
	StatusDialogPrivate(CStatusDialog *_public);
};
// struct StatusDialogPrivate


StatusDialogPrivate::StatusDialogPrivate(CStatusDialog *_public) :
	_this(_public)
{

}


CStatusDialog::CStatusDialog(Qx::DockManager* dockManager) :
	QDialog(dockManager),
	d(new StatusDialogPrivate(this))
{
	d->ui.setupUi(this);
	d->dockManager = dockManager;
	d->DockWidgets = dockManager->dockWidgetsMap();

	for (auto it = d->DockWidgets.begin(); it != d->DockWidgets.end(); ++it)
	{
		QVariant vDockWidget = QVariant::fromValue(it.value());
		d->ui.dockWidgetsComboBox->addItem(it.key(), vDockWidget);
	}
}


CStatusDialog::~CStatusDialog()
{
	delete d;
}



void CStatusDialog::on_dockWidgetsComboBox_currentIndexChanged(int index)
{
	if (index < 0)
	{
		return;
	}

	auto vDockWidget = d->ui.dockWidgetsComboBox->currentData();
	auto dockWidget = vDockWidget.value<Qx::DockWidget*>();
	d->ui.isClosedCheckBox->setChecked(dockWidget->isClosed());
	d->ui.isFloatingCheckBox->setChecked(dockWidget->isFloating());
	d->ui.tabbedCheckBox->setChecked(dockWidget->isTabbed());
	d->ui.isCurrentTabCheckBox->setChecked(dockWidget->isCurrentTab());
	d->ui.closableCheckBox->setChecked(dockWidget->features().testFlag(Qx::DockWidget::DockWidgetClosable));
	d->ui.movableCheckBox->setChecked(dockWidget->features().testFlag(Qx::DockWidget::DockWidgetMovable));
	d->ui.floatableCheckBox->setChecked(dockWidget->features().testFlag(Qx::DockWidget::DockWidgetFloatable));
	d->ui.deleteOnCloseCheckBox->setChecked(dockWidget->features().testFlag(Qx::DockWidget::DockWidgetDeleteOnClose));
	d->ui.customCloseHandlingCheckBox->setChecked(dockWidget->features().testFlag(Qx::DockWidget::CustomCloseHandling));
}

//---------------------------------------------------------------------------
// EOF StatusDialog.cpp
