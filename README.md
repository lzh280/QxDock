# 前言

QxDock 是一款 Advanced Docking System for Qt 组件，改自 [Qt-Advanced-Docking-System](https://github.com/githubuser0xFFFF/Qt-Advanced-Docking-System)

# 仓库
- gitee：[https://gitee.com/icanpool/QxDock](https://gitee.com/icanpool/QxDock)

# 协议
* 遵循 [LGPL-2.1](./LICENSE) 开源许可协议

# 规范
* [Google C++ Style Guide](http://google.github.io/styleguide/cppguide.html)
* [Qt 编程风格与规范](https://blog.csdn.net/qq_35488967/article/details/70055490)
* 源文件全英文的采用 UTF-8 编码，包含中文的采用 UTF-8 with BOM 编码
* 代码 git 提交格式：[git 知：提交格式](https://blog.csdn.net/canpool/article/details/126005367)

# 贡献
* 欢迎提交 issue 对关心的问题发起讨论
* 欢迎 Fork 仓库，pull request 贡献
* 贡献者可在文件头版权中添加个人信息，格式如下：
```
/**
 * Copyleft (C) YYYY NAME EMAIL
 * Copyleft (C) 2023 maminjie <canpool@163.com>
 * SPDX-License-Identifier: LGPL-2.1
**/
```

# 交流
* QQ群：831617934（Qt业余交流）

# 演示
- <font size=4>dock demo（Windows 10）</font>

![](./doc/images/dockdemo.png)

- <font size=4>dock demo（openEuler 22.03 LTS SP1）</font>

![](./doc/images/dockdemo_linux.png)

备注：安装 GNOME 桌面后，需要安装如下 qt5 包
```
dnf install qt5 qt5-devel qt5-qtbase-private-devel -y
```

# 后语
QxDock 主要是作者用于研究，建议用户使用 Qt-Advanced-Docking-System
